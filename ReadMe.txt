Pour lancer le projet,

1- Dans bloup, aller dans le repertoire Bloupapp (repertoire qui contient le react)
2- Lancer npm install dans le repertoire Bloupapp
3- Lancer npm run build dans le repertoire Bloupapp
4- D�placer le contenu de build dans Bloupapp dans le repertoire src/main/resources/static
5- Lancer l'application "has a spring boot app"

L'application se lance sur le port 80. L'addresse ip pour voir le projet sur amazone est le 107.20.79.1

PS. L'utilisateur "asd@asd.com" est deja existant (hardcoder a des fins de test). Son mot de passe est "Asd1".
Sinon, ne pas oublier que quand vous creez un utilisateur, vous devez aller valider son Email pour pouvoir l'activer
Si l'application venait a etre down sur amazone, juste lancer le jar bloupwebapp-0.test.jar
La commande : sudo nohup java -jar BloupWebApp-0.test.jar &