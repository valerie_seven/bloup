package com.controllers;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import com.domaine.User;
import com.service.LoginUtil;

@CrossOrigin(origins = "*", allowCredentials = "true", allowedHeaders = "*")
@org.springframework.web.bind.annotation.RestController
public class LoginController {
	
	@Autowired
	private LoginUtil _LoginUtil;
	
	@RequestMapping(value ="/Login/", method = RequestMethod.POST)
    public User VerifyLogin(HttpServletRequest request, HttpSession httpSession){
		return _LoginUtil.verifyLogin(
				request.getParameter("email"), 
				request.getParameter("password"));
    }
    
    @RequestMapping(value = "/Register/ActivateUser", method=RequestMethod.POST)
    public String ActivateTmpUser(@RequestParam Map<String, String> params) {
    	System.out.println("confirmation id : " + params.get("tmpid"));
    	return _LoginUtil.ActivateUser(params.get("tmpid"));
    }
}
