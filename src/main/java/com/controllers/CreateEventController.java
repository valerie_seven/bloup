package com.controllers;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.service.event.CreateEvent;
import com.utilitaires.ConvertDate;

import static com.utilitaires.Constante.URL_CREATE_BASIC_ALLDAY_EVENT;
import static com.utilitaires.Constante.URL_CREATE_COMPLEX_ALLDAY_EVENT;
import static com.utilitaires.Constante.URL_CREATE_BASIC_EVENT;
import static com.utilitaires.Constante.URL_CREATE_COMPLEX_EVENT;
import static com.utilitaires.Constante.PARAMS_TITLE;
import static com.utilitaires.Constante.PARAMS_STARTING_DATE;
import static com.utilitaires.Constante.PARAMS_ENDING_DATE;
import static com.utilitaires.Constante.PARAMS_IS_REPETITIVE;
import static com.utilitaires.Constante.PARAMS_REPETITION_TYPE;
import static com.utilitaires.Constante.PARAMS_COMMENT;
import static com.utilitaires.Constante.PARAMS_USER_EMAIL;

@CrossOrigin
@RestController
public class CreateEventController {

	@Autowired
    private CreateEvent createEvent;
    
	//private ConvertDate date = new ConvertDate();
	
    @RequestMapping(value = URL_CREATE_BASIC_ALLDAY_EVENT, method=RequestMethod.POST)
    public boolean createBasicAlldayEvent(@RequestParam Map<String, String> params) {
    	return createEvent.createBasicAllDayEvent(
			params.get(PARAMS_TITLE), 
			ConvertDate.stringToDate(params.get(PARAMS_STARTING_DATE)), 
			params.get(PARAMS_USER_EMAIL));
    }

    @RequestMapping(value = URL_CREATE_BASIC_EVENT, method=RequestMethod.POST)
    public boolean createBasicEvent(@RequestParam Map<String, String> params) {
    	return createEvent.createBasicEvent(
			params.get(PARAMS_TITLE),
			ConvertDate.stringToDate(params.get(PARAMS_STARTING_DATE)), 
			ConvertDate.stringToDate(params.get(PARAMS_ENDING_DATE)), 
			params.get(PARAMS_USER_EMAIL));
    }
    
    @RequestMapping(value = URL_CREATE_COMPLEX_ALLDAY_EVENT, method=RequestMethod.POST)
    public boolean createCompleteAlldayEvent(@RequestParam Map<String, String> params) {    	
    	return createEvent.createCompleteAllDayEvent(
			params.get(PARAMS_TITLE),
			ConvertDate.stringToDate(params.get(PARAMS_STARTING_DATE)),
			Boolean.getBoolean(params.get(PARAMS_IS_REPETITIVE)),
			Integer.getInteger(params.get(PARAMS_REPETITION_TYPE)),
			params.get(PARAMS_COMMENT),
			params.get(PARAMS_USER_EMAIL));
    }
    
    @RequestMapping(value = URL_CREATE_COMPLEX_EVENT, method=RequestMethod.POST)
    public boolean createCompleteEvent(@RequestParam Map<String, String> params) {
    	return createEvent.createCompleteEvent(
			params.get(PARAMS_TITLE),
			ConvertDate.stringToDate(params.get(PARAMS_STARTING_DATE)),
			ConvertDate.stringToDate(params.get(PARAMS_ENDING_DATE)),
			Boolean.getBoolean(params.get(PARAMS_IS_REPETITIVE)),
			Integer.getInteger(params.get(PARAMS_REPETITION_TYPE)),
			params.get(PARAMS_COMMENT),
			params.get(PARAMS_USER_EMAIL));
    }
}
