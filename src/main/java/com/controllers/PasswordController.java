package com.controllers;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.service.PasswordCRUD;

@CrossOrigin
@RestController
public class PasswordController {
	
	@Autowired
	PasswordCRUD _passwordCRUD;
	
	@RequestMapping(value = "/Password/updatePassword", method=RequestMethod.POST)
    public String updateComments(@RequestParam Map<String, String> params) {
        return _passwordCRUD.updatePassword(
        		params.get("email"), 
        		params.get("currentPWD"), 
        		params.get("newPWD"), 
        		params.get("confirmPWD"));
    }
}
