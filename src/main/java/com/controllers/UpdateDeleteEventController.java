package com.controllers;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.service.event.UpdateEvent;
import com.utilitaires.ConvertDate;

import static com.utilitaires.Constante.URL_UPDATE_DATE_EVENT;
import static com.utilitaires.Constante.URL_UPDATE_EVENT;
import static com.utilitaires.Constante.URL_DELETE_EVENT;
import static com.utilitaires.Constante.PARAMS_EVENT_ID;
import static com.utilitaires.Constante.PARAMS_TITLE;
import static com.utilitaires.Constante.PARAMS_STARTING_DATE;
import static com.utilitaires.Constante.PARAMS_ENDING_DATE;
import static com.utilitaires.Constante.PARAMS_IS_REPETITIVE;
import static com.utilitaires.Constante.PARAMS_REPETITION_TYPE;
import static com.utilitaires.Constante.PARAMS_COMMENT;
import static com.utilitaires.Constante.PARAMS_USER_EMAIL;

@CrossOrigin
@RestController
public class UpdateDeleteEventController {

	@Autowired
    private UpdateEvent updateEvent;
	
	@RequestMapping(value = URL_UPDATE_DATE_EVENT, method=RequestMethod.POST)
    public boolean updateDateEvent(
    		@RequestParam Map<String, String> params) {
		return updateEvent.saveUpdatedDatesEvent(
			Long.parseLong(params.get(PARAMS_EVENT_ID)),
			ConvertDate.stringToDate(params.get(PARAMS_STARTING_DATE)),
			ConvertDate.stringToDate(params.get(PARAMS_ENDING_DATE)),
			params.get(PARAMS_USER_EMAIL));
    }
	
    @RequestMapping(value = URL_UPDATE_EVENT, method=RequestMethod.POST)
    public boolean updateEvent(
    		@RequestParam Map<String, String> params) {
    	return updateEvent.saveUpdatedEvent(
			Long.parseLong(params.get(PARAMS_EVENT_ID)),
			params.get(PARAMS_TITLE),
			ConvertDate.stringToDate(params.get(PARAMS_STARTING_DATE)),
			ConvertDate.stringToDate(params.get(PARAMS_ENDING_DATE)),
			Boolean.getBoolean(params.get(PARAMS_IS_REPETITIVE)),
			Integer.parseInt(params.get(PARAMS_REPETITION_TYPE)),
			params.get(PARAMS_COMMENT),
			params.get(PARAMS_USER_EMAIL));
    }
	
    @RequestMapping(value = URL_DELETE_EVENT, method=RequestMethod.POST)
	public boolean deleteEvent(@RequestParam Map<String, String> params) {
		return updateEvent.deleteEvent(Long.parseLong(params.get(PARAMS_EVENT_ID)));
	}
}
