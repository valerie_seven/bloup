package com.controllers;

import static com.utilitaires.Constante.URL_GET_FACT_DIDYOUKNOW;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.service.DidYouKnowService;

@CrossOrigin
@RestController
public class DidYouKnowController {

	@Autowired
	public DidYouKnowService didYouKnowService;
	
	@RequestMapping(value = URL_GET_FACT_DIDYOUKNOW, method=RequestMethod.POST)
    public String getRandomDidYouKnowFact() {
    	return didYouKnowService.getFact();
    }
}
