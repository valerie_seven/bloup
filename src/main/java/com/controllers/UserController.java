package com.controllers;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.service.UserCRUD;

@CrossOrigin
@RestController
public class UserController {
	@Autowired
	private UserCRUD _userCRUD;
	
	@RequestMapping(value = "/User/createUser", method=RequestMethod.POST)
    public String RegisterUser(@RequestParam Map<String, String> params) {
    	return _userCRUD.registerUser(
    			params.get("email"), 
    			params.get("password"), 
    			params.get("passconf"), 
    			params.get("firstName"), 
    			params.get("lastName"));
    }
	
	@RequestMapping(value = "/User/updateUserName", method=RequestMethod.POST)
    public String updateUserName(@RequestParam Map<String, String> params) {
        return _userCRUD.updateUserName(
        		params.get("email"), 
        		params.get("firstName"), 
        		params.get("lastName"));
    }
}
