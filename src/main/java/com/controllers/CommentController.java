package com.controllers;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.domaine.Comment;
import com.service.CommentCRUD;

@CrossOrigin
@RestController
public class CommentController {
	
	@Autowired
	private CommentCRUD _commentCRUD;
	
	@RequestMapping(value = "/Comment/createComment", method=RequestMethod.POST)
    public Boolean createComments(@RequestParam Map<String, String> params) {
        return _commentCRUD.createComment(
        		params.get("comment"),
        		params.get("email"));
    }
	
	@RequestMapping(value = "/Comment/updateComment", method=RequestMethod.POST)
    public Boolean updateComments(@RequestParam Map<String, String> params) {
        return _commentCRUD.updateComment(
        		Long.parseLong(params.get("id")),
        		params.get("comment"));
    }
	
	@RequestMapping(value = "/Comment/deleteComment", method=RequestMethod.POST)
    public Boolean deleteComments(@RequestParam Map<String, String> params) {
        return _commentCRUD.deleteComment(Long.parseLong(params.get("id")));
    }
	
	@RequestMapping(value = "/Comment/getByID", method=RequestMethod.POST)
    public Comment getCommentById(@RequestParam Map<String, String> params) {
        return _commentCRUD.getCommentId(Long.parseLong(params.get("id")));
    }
	
	@RequestMapping(value = "/Comment/getByEmail", method=RequestMethod.POST)
    public List<Comment> getCommentByEmail(@RequestParam Map<String, String> params) {
        return _commentCRUD.getCommentsByEmail(params.get("email"));
    }
}
