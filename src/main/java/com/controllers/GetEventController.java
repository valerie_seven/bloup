package com.controllers;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.domaine.Event;
import com.service.event.ShowEvent;
import com.utilitaires.ConvertDate;

import static com.utilitaires.Constante.URL_FIND_BY_EVENT_ID;
import static com.utilitaires.Constante.URL_FIND_BY_USER_EMAIl;
import static com.utilitaires.Constante.URL_FIND_ALL;
import static com.utilitaires.Constante.URL_FIND_BY_TITLE;
import static com.utilitaires.Constante.URL_FIND_BY_EVENT_DATE;
import static com.utilitaires.Constante.PARAMS_EVENT_ID;
import static com.utilitaires.Constante.PARAMS_TITLE;
import static com.utilitaires.Constante.PARAMS_STARTING_DATE;
import static com.utilitaires.Constante.PARAMS_USER_EMAIL;

@CrossOrigin
@RestController
public class GetEventController {
	
	@Autowired
    private ShowEvent afficheEvent;
	
	//private ConvertDate date = new ConvertDate();
    
    
    @RequestMapping(value = URL_FIND_BY_EVENT_ID, method=RequestMethod.POST)
    public Event getById(@RequestParam Map<String, String> params) {
    	return afficheEvent.getEventByEventId(Long.parseLong(params.get(PARAMS_EVENT_ID)));
    }
    
    @RequestMapping(value = URL_FIND_BY_USER_EMAIl, method=RequestMethod.POST)
    public List<Event> getAllEventsByUserEmail(@RequestParam Map<String, String> params) {
    	return afficheEvent.getAllEventsOfUser(params.get(PARAMS_USER_EMAIL));
    }
    
    @RequestMapping(value = URL_FIND_ALL, method=RequestMethod.POST)
    public List<Event> getAllEvents() {
    	return afficheEvent.getAllEvents();
    }
    
    @RequestMapping(value = URL_FIND_BY_TITLE, method=RequestMethod.POST)
    public List<Event> getAllEventsByEventTitleAndUserEmail(
    		@RequestParam Map<String, String> params) {
    	return afficheEvent.getAllEventsByEventTitleAndUserEmail(
			params.get(PARAMS_TITLE),
			params.get(PARAMS_USER_EMAIL));
    }
    
    @RequestMapping(value = URL_FIND_BY_EVENT_DATE, method=RequestMethod.POST)
    public List<Event> getAllEventsByEventDateAndUserEmail(
    		@RequestParam Map<String, String> params) {
    	return afficheEvent.getEventsByEventStartingDateAndUserEmail(
    		ConvertDate.stringToDate(params.get(PARAMS_STARTING_DATE)),
			params.get(PARAMS_USER_EMAIL));
    }
}