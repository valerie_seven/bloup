package com;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;

import com.demo.CreateAsdStartThread;

@SpringBootApplication
@ComponentScan("com"/*basePackages={"com/controllers", "com/repository"}*/)
public class BloupWebAppApplication {

	public static void main(String[] args) {
		SpringApplication.run(BloupWebAppApplication.class, args);
	}
	
	//TODO Supprimer de la version finale
	/**
	 * À des fins de Test uniquement
	 */
	
	@Bean
    public CommandLineRunner demo(CreateAsdStartThread service) {
        return (args) -> {
            service.initializeDbAndCreateThread();
        };
    }
}
