
package com.domaine;

import java.io.File;
import java.util.ArrayList;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="BLOUBFILE")
public class BloupFile {
	
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name = "bfil_UserId", nullable = false, updatable = false)
	@Id
	private int _id;
	@Column(name="FK_eventId")
	private int _eventId;
	@Column(name="bfil_FileName")
	private String _FileName;
	@Column(name="bfil_bloupFile")
	private File _file;
	@Column(name="bfil_FileSubversion")
	private ArrayList<File> _subversions;
	
	public BloupFile(int _eventId, String _FileName, File _file, ArrayList<File> _subversions) {
		super();
		this._eventId = _eventId;
		this._FileName = _FileName;
		this._file = _file;
		this._subversions = _subversions;
	}
	
	public int get_id() {
		return _id;
	}
	
	public int get_eventId() {
		return _eventId;
	}
	
	public void set_eventId(int _eventId) {
		this._eventId = _eventId;
	}
	
	public String get_FileName() {
		return _FileName;
	}
	
	public void set_FileName(String _FileName) {
		this._FileName = _FileName;
	}
	
	public File get_file() {
		return _file;
	}
	
	public void set_file(File _file) {
		this._file = _file;
	}
	
	public ArrayList<File> get_subversions() {
		return _subversions;
	}
	
	public void set_subversions(ArrayList<File> _subversions) {
		this._subversions = _subversions;
	}
	
	@Override
	public String toString() {
		return "BloupFile [id=" + _id + ", _FileName=" + _FileName + ", _file=" + _file + ", _subversions="
				+ _subversions + "]";
	}
}
