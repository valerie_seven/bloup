package com.domaine;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import static com.utilitaires.Constante.*;

@Entity
@Table(name="blp_event")
public class Event {

	@GeneratedValue(strategy=GenerationType.IDENTITY)
    @Column(name = "eventid", nullable = false, updatable = false)
	@Id
    private Long _eventId;
    
	@Column(name="_eventtitle")
	private String _eventTitle;
    @Column(name="eventstartingdate")
	private Date _eventStartingDate;
    @Column(name="eventendingdate")
	private Date _eventEndingDate;
    @Column(name="isrepetitive")
	private boolean _isRepetitive;
    @Column(name="repititiontype")
	private int _repititionType;
    @Column(name="comments")
	private String _comments;
	
    @Column(name="emailUser")
	private String _emailUser;

	public Event() {
        super();
        this._eventTitle = EVENT_NO_TITLE;
        this._eventStartingDate = new Date();
        this._eventEndingDate   = new Date();
        this._isRepetitive = EVENT_IS_NOT_REPEATING;
        this._repititionType = EVENT_NO_REPETITIONS;
        this._comments = EVENT_NO_COMMENTS;
    }
	
    public Event(String eventTitle, Date eventStartingDate, 
    		Date eventEndingDate, String userEmail) {
        super();
        this._eventTitle = eventTitle;
        this._eventStartingDate = eventStartingDate;
        this._eventEndingDate = eventEndingDate;
        this._emailUser = userEmail;
        this._isRepetitive = EVENT_IS_NOT_REPEATING;
        this._repititionType = EVENT_NO_REPETITIONS;
        this._comments = EVENT_NO_COMMENTS;
    }

    public Event(String eventTitle, Date eventStartingDate, Date eventEndingDate, 
    		boolean isRepetitive, int repititionType, String comments, String userEmail) {
        super();
        this._eventTitle = eventTitle;
        this._eventStartingDate = eventStartingDate;
        this._eventEndingDate = eventEndingDate;
        this._isRepetitive = isRepetitive;
        this._repititionType = repititionType;
        this._comments = comments;
        this._emailUser = userEmail;
    }

    public Long getEventId() {
        return _eventId;
    }

    public void setEventId(Long eventId) {
        this._eventId = eventId;
    }
    
    public String get_title() {
		return _eventTitle;
	}

	public void set_title(String _titre) {
		this._eventTitle = _titre;
	}

	public Date getEventStartingDate() {
        return _eventStartingDate;
    }

    public void setEventStartingDate(Date eventStartingDate) {
        this._eventStartingDate = eventStartingDate;
    }

    public Date getEventEndingDate() {
        return _eventEndingDate;
    }

    public void setEventEndingDate(Date eventEndingDate) {
        this._eventEndingDate = eventEndingDate;
    }

    public boolean isRepetitive() {
        return _isRepetitive;
    }

    public void setRepetitive(boolean isRepetitive) {
        this._isRepetitive = isRepetitive;
    }

    public int getRepititionType() {
        return _repititionType;
    }

    public void setRepititionType(int repititionType) {
        this._repititionType = repititionType;
    }

    public String getComments() {
        return _comments;
    }

    public void setComments(String comments) {
        this._comments = comments;
    }

	public String get_email() {
		return _emailUser;
	}

	public void set_email(String _email) {
		this._emailUser = _email;
	}

	@Override
	public String toString() {
		return "Event [_eventId=" + _eventId + ", _eventTitle=" + _eventTitle + ", _eventDateDebut=" + _eventStartingDate
				+ ", _eventDateFin=" + _eventEndingDate + ", _isRepetitive=" + _isRepetitive + ", _repititionType="
				+ _repititionType + ", _comments=" + _comments + "]";
	}
    
}