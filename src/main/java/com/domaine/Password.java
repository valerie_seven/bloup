package com.domaine;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="blp_Password_pwd")
public class Password {

	@Column(name = "pwd_email", nullable = false, updatable = false)
	@Id
	private String _email;
	@Column(name="pwd_password")
	private String _password;
	
	public Password() {
		super();
	}
	
	public Password(String _email, String _password) {
		super();
		this._email = _email;
		this._password = _password;
	}
	
	public String get_email() {
		return _email;
	}
	public void set_userMail(String _email) {
		this._email = _email;
	}
	
	public String get_password() {
		return _password;
	}
	public void set_password(String _password) {
		this._password = _password;
	}
}
