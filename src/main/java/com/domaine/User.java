package com.domaine;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="blp_user_usr")
public class User {
		
	@Column(name="email", unique=true, nullable = false)
	@Id
	private String _email;
	@Column(name="firstName")
	private String _firstName;
	@Column(name="lastName")
	private String _lastName;
	
    public User() {
		super();
	}
    
	public User(String email, String firstName, String lastName) {
		super();
		this._email = email;
		this._firstName = firstName;
		this._lastName = lastName;
	}
	
	public String getEmail() {
		return _email;
	}
	public void setEmail(String email) {
		this._email = email;
	}
	public String getFirstName() {
		return _firstName;
	}
	public void setFirstName(String firstName) {
		this._firstName = firstName;
	}
	public String getLastName() {
		return _lastName;
	}
	public void setLastName(String lastName) {
		this._lastName = lastName;
	}

	@Override
	public String toString() {
		return "User [_email=" + _email + ", _firstName=" + _firstName +
				", _lastName=" + _lastName + ", _events=" + "]";
	}
}
