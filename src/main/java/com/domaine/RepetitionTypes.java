package com.domaine;

public enum RepetitionTypes {
	once,
	daily,
	weekly,
	monthly,
	yearly
};
