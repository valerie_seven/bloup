package com.domaine;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="blp_TmpUserList")
public class TemporaryUser {
	
	@Id
	@Column(name="tmpCodeId")
	private String _tmpCodeId;
	@Column(name="email", unique=true, nullable = false)
	private String _email;
	@Column(name="firstName")
	private String _firstName;
	@Column(name="lastName")
	private String _lastName;
	@Column(name="password")
	private String _password;
	
	public TemporaryUser() {
		super();
	}
	
	public TemporaryUser(String _email, String _firstName, String _lastName,String _password) {
		super();
		this._email = _email;
		this._firstName = _firstName;
		this._lastName = _lastName;
		this._password = _password;
		String tmpCode = "";
		for (int i = 0; i < 20; i++) tmpCode += ""+(int)(Math.random()*10);
		this._tmpCodeId = tmpCode;
	}

	public String get_tmpCodeId() {
		return _tmpCodeId;
	}

	public void set_tmpCodeId(String _tmpCodeId) {
		this._tmpCodeId = _tmpCodeId;
	}

	public String get_email() {
		return _email;
	}

	public void set_email(String _email) {
		this._email = _email;
	}

	public String get_firstName() {
		return _firstName;
	}

	public void set_firstName(String _firstName) {
		this._firstName = _firstName;
	}

	public String get_lastName() {
		return _lastName;
	}

	public void set_lastName(String _lastName) {
		this._lastName = _lastName;
	}

	public String get_password() {
		return _password;
	}

	public void set_password(String _password) {
		this._password = _password;
	}
	
}
