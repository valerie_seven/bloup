package com.domaine;

import java.text.SimpleDateFormat;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "blp_Comment")
public class Comment {
	
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name="id", nullable = false, updatable = false)
	@Id
	long _id;
	
	
	private String _email;
	
	@Column(name="comment")
	private String _comment;
	
	@Column(name="date")
	private String _date;
	
	public Comment() {
		super();
	}

	public Comment(String _comment, String _email) {
		super();
		this._comment = _comment;
		this._email = _email;
		this._date = new SimpleDateFormat("dd/MM/yyyy").format(new Date());
	}

	
	
	public long get_id() {
		return _id;
	}
	public void set_id(long _id) {
		this._id = _id;
	}

	public String get_comment() {
		return _comment;
	}
	public void set_comment(String _comment) {
		this._comment = _comment;
	}

	public String get_email() {
		return _email;
	}
	public void set_email(String _email) {
		this._email = _email;
	}

	public String get_date() {
		return _date;
	}
	public void set_date(String _date) {
		this._date = _date;
	}

	
	@Override
	public String toString() {
		return "Comment [_id=" + _id + ", _email=" + _email + ", _comment=" + _comment + ", _date=" + _date + "]";
	}
	
}
