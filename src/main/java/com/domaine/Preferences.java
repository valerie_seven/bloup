package com.domaine;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "PREFERENCES")
public class Preferences {

	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "pref_UserId", nullable = false, updatable = false)
	@Id
	private int _userId;
	@Column(name = "pref_hasEmailNotification")
	private boolean _hasEmailNotification;
	@Column(name = "pref_hasMobileAlert")
	private boolean _receiveMobileAlert;
	@Column(name = "pref_displayMode")
	private RepetitionTypes _displayMode;

	public int getId() {
		return _userId;
	}

	public boolean is_receiveMailNotification() {
		return _hasEmailNotification;
	}

	public void set_receiveMailNotification(boolean _receiveMailNotification) {
		this._hasEmailNotification = _receiveMailNotification;
	}

	public boolean is_receiveMobileAlert() {
		return _receiveMobileAlert;
	}

	public void set_receiveMobileAlert(boolean _receiveMobileAlert) {
		this._receiveMobileAlert = _receiveMobileAlert;
	}

	@Override
	public String toString() {
		return "Preferences [id=" + _userId + ", _receiveMailNotification=" + _hasEmailNotification
				+ ", _receiveMobileAlert=" + _receiveMobileAlert + "]";
	}
}
