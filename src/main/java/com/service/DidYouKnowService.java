package com.service;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.Scanner;

import org.springframework.stereotype.Service;

import static com.utilitaires.Constante.HARDCODED_FACT;
import static com.utilitaires.Constante.PATH_DID_YOU_KNOW_FILE;

@Service
public class DidYouKnowService {
	
	private List<String> factList;
	
	public String getFact() {
		return getRandomFact();
	}

	private DidYouKnowService() {
		factList = new ArrayList<>();
		try {
			String line = null;
			URL pathFile = DidYouKnowService.class.getResource(PATH_DID_YOU_KNOW_FILE);
			File f = new File(pathFile.getFile());
			BufferedReader br = new BufferedReader(new FileReader(f));
			while((line = br.readLine()) != null) {
				factList.add(line);
			}
			br.close();
		} catch (Exception e) {
			try {
				File file = new File(PATH_DID_YOU_KNOW_FILE);
				Scanner scan = new Scanner(file);
				while(scan.hasNextLine()) {
					factList.add(scan.nextLine());
				}
				scan.close();
			} catch (Exception e2) {
				e2.printStackTrace();
				factList.add(HARDCODED_FACT);
			}
			
		}
	}
	
	private String getRandomFact() {
		return factList.get(new Random().nextInt(factList.size()));
	}
}
