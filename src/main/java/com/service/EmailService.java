package com.service;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Properties;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

import com.domaine.Event;
import com.domaine.TemporaryUser;
import com.service.event.ShowEvent;

public class EmailService {
	
	
	public static void notifyForIncomingEvents() {
		com.service.event.ShowEvent eventFetcher = new ShowEvent();
		List<Event> listeEvent = eventFetcher.getAllEvents();
		Date now = new Date();
		now.setHours(now.getHours() + 1);
		for (Event event : listeEvent) {
			if(now.after(event.getEventStartingDate()))
				sendEventNotifyMail(event.get_email(), event);
		}
	}
	
	public static void sendRegisterMail(String email, TemporaryUser user) {
		try {
			generateAndSendEmail(email, null, user);
		} catch (AddressException e) {
			e.printStackTrace();
		} catch (MessagingException e) {
			e.printStackTrace();
		}
	}
	
	public static void sendEventNotifyMail(String email, Event event) {
		try {
			generateAndSendEmail(email, event, null);
		} catch (AddressException e) {
			e.printStackTrace();
		} catch (MessagingException e) {
			e.printStackTrace();
		}
	}
	
	private static void generateAndSendEmail(String email, Event event, TemporaryUser user) throws AddressException, MessagingException {
		
		Properties mailServerProperties;
		Session getMailSession;
		MimeMessage generateMailMessage;
		 
		// Step1
		mailServerProperties = setUpMailServer();
 
		// Step2
		System.out.println("\n\n 2nd ===> get Mail Session..");
		getMailSession = Session.getDefaultInstance(mailServerProperties, null);
		generateMailMessage = initEmail(email, event, user, getMailSession);
		System.out.println("Mail Session has been created successfully..");
 
		// Step3
		System.out.println("\n\n 3rd ===> Get Session and Send mail");
		Transport transport = getMailSession.getTransport("smtp");
 
		// Enter your correct gmail UserID and Password
		// if you have 2FA enabled then provide App Specific Password
		transport.connect("smtp.gmail.com", "bloupcalendar@gmail.com", "bloupbloup");
		transport.sendMessage(generateMailMessage, generateMailMessage.getAllRecipients());
		transport.close();
	}

	private static MimeMessage initEmail(String email, Event event, TemporaryUser user, Session getMailSession)
			throws MessagingException, AddressException {
		MimeMessage generateMailMessage;
		if(user != null && event == null)
			generateMailMessage = generateMailContent(getMailSession, email, user);
		else if(user == null && event != null)
			generateMailMessage = generateMailContent(getMailSession, email, event);
		else 
			generateMailMessage = null;
		return generateMailMessage;
	}

	private static MimeMessage generateMailContent(Session getMailSession, String email, TemporaryUser user) throws MessagingException, AddressException {
		MimeMessage generateMailMessage;
		generateMailMessage = new MimeMessage(getMailSession);
		generateMailMessage.addRecipient(Message.RecipientType.TO, new InternetAddress(email));
		generateMailMessage.setSubject("Bienvenue sur Bloup!");
		String emailBody = "Vous avez creer un compte avec succès!" + "<br><br> Il ne vous reste qu'à activer votre compte en cliquant sur le lien suivant :<br>"+ generateRandomLink(user) +"<br><br> Merci!,<br>l'equipe Bloup";
		generateMailMessage.setContent(emailBody, "text/html");
		return generateMailMessage;
	}
	
	private static String generateRandomLink(TemporaryUser user) {
		//return "http://localhost/?id=" + user.get_tmpCodeId();
		return "http://107.20.79.1/?id=" + user.get_tmpCodeId();
	}

	private static MimeMessage generateMailContent(Session getMailSession, String email, Event event) throws MessagingException, AddressException {
		MimeMessage generateMailMessage;
		DateFormat dateFormat = new SimpleDateFormat("HH:mm:ss");
		generateMailMessage = new MimeMessage(getMailSession);
		generateMailMessage.addRecipient(Message.RecipientType.TO, new InternetAddress(email));
		generateMailMessage.setSubject("Memo pour votre evenement :" + event.get_title());
		
		String emailBody = "Votre evenement :" + event.get_title() + "<br>is starting at : " + 
		dateFormat.format(event.getEventStartingDate()) + ", <br>Crunchify Admin";
		
		generateMailMessage.setContent(emailBody, "text/html");
		return generateMailMessage;
	}

	private static Properties setUpMailServer() {
		Properties mailServerProperties;
		System.out.println("\n 1st ===> setup Mail Server Properties..");
		mailServerProperties = System.getProperties();
		mailServerProperties.put("mail.smtp.port", "587");
		mailServerProperties.put("mail.smtp.auth", "true");
		mailServerProperties.put("mail.smtp.starttls.enable", "true");
		System.out.println("Mail Server Properties have been setup successfully..");
		return mailServerProperties;
	}
}
