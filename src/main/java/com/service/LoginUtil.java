package com.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.domaine.TemporaryUser;
import com.domaine.User;

@Service
public class LoginUtil {
	@Autowired
	private UserCRUD _UserCrud;
	
	@Autowired
	private PasswordCRUD _PasswordCrud;
	
	@Autowired
	private TmpUserCrud _TmpUserCrud;
	
	// TODO return a error message if login fails, "" otherwise
	public User verifyLogin(String email, String password) {
		if (_UserCrud.userExist(email)) {
			String goodPassword = _PasswordCrud.getPasswordByEmail(email).get_password();
			if (PasswordUtil.isGoodPassword(password, goodPassword))
				return _UserCrud.getUserByEmail(email);
		}
		return null;
	}
	
	public String ActivateUser(String id) {
		TemporaryUser tmpUser = _TmpUserCrud.getUserByTmpId(id);
		if(tmpUser != null && !_UserCrud.userExist(tmpUser.get_email())) { 
			_UserCrud.createUser(tmpUser.get_email(),
					tmpUser.get_password(),
					tmpUser.get_firstName(),
					tmpUser.get_lastName());
		}
		else {
			System.out.println("le user est null");
		}
		return null;
	}

}
