package com.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.domaine.Password;
import com.repository.PasswordRepository;

import static com.utilitaires.Constante.ERROR_PASSWORDS_DONT_MATCH;
import static com.utilitaires.Constante.ERROR_PASSWORD_INVALID;
import static com.utilitaires.Constante.ERROR_WRONG_PASSWORD;

@Service
public class PasswordCRUD {

	@Autowired
    private PasswordRepository _PasswordRepository;
	
	public String updatePassword(String email, String currentPassword, String newPassword, String confirmNewPassword) { 
		Password password = getPasswordByEmail(email);
		
		if(PasswordUtil.isGoodPassword(currentPassword, password.get_password())) {
			if(PasswordUtil.isConfirmPasswordMatch(newPassword, confirmNewPassword)) {
				if(PasswordUtil.isPasswordValid(newPassword)) {
					password.set_password(PasswordUtil.encryptPassword(newPassword));
					_PasswordRepository.save(password);
					return "Le mot de passe a ete change";
				}
				return ERROR_PASSWORD_INVALID;
			}
			return ERROR_PASSWORDS_DONT_MATCH;
		}	
		return ERROR_WRONG_PASSWORD;
    }
	
	public boolean deletePassword(String email, String password) { 
		
		return true;
    }
	
	public Password getPasswordByEmail(String email) {
		List<Password> result =  _PasswordRepository.findBy_email(email);
		if(!result.isEmpty())
			return result.get(0);
		return null;
	}
}
