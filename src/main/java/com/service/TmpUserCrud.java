package com.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.domaine.TemporaryUser;
import com.domaine.User;
import com.repository.TmpUserRepository;

@Service
public class TmpUserCrud {
	@Autowired 
    private TmpUserRepository _tmpUserRepository; 

	public void createTmpUser(String email, String firstName, String lastName, String User_password) { 
		TemporaryUser tmpUser = new TemporaryUser(email, firstName, lastName, User_password);
		_tmpUserRepository.save(tmpUser);
		EmailService.sendRegisterMail(email, tmpUser);
    } 
  
	public TemporaryUser getUserByTmpId(String id) { 
		List<TemporaryUser> result = _tmpUserRepository.findBy_tmpCodeId(id); 
		if(!result.isEmpty()) 
			return result.get(0); 
		return null; 
	} 
	
	public boolean deleteTmpUser(String _email, String _password) { 
		//ToDo
		return false;
    }
}
