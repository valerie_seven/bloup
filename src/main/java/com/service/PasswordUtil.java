package com.service;

import static com.utilitaires.Constante.PATTERN_VERIFY_PASSWORD;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class PasswordUtil {
	
	private static Logger logger = Logger.getLogger(PasswordUtil.class.getName());
	private static MessageDigest md;
	
	public static String encryptPassword(String password) {	
		try {
	        md = MessageDigest.getInstance("MD5");
	        byte[] passBytes = password.getBytes();
	        md.reset();
	        byte[] digested = md.digest(passBytes);
	        StringBuffer sb = new StringBuffer();
	        
	        for(int i=0;i<digested.length;i++)
	            sb.append(Integer.toHexString(0xff & digested[i]));
	        
	        return sb.toString();
	    } 
		catch (NoSuchAlgorithmException ex) {
	    	logger.log(null, "The Encryption Encontered an error");
	    }
        return null;
   }
	
	public static boolean isPasswordValid(String password) {
		Pattern pattern = Pattern.compile(PATTERN_VERIFY_PASSWORD);
		Matcher matcher = pattern.matcher(password);
		if (matcher.find())
			return true;
		return false;
	}
	
	public static boolean isConfirmPasswordMatch(String password, String confirmPassword) {
		return password.equals(confirmPassword);
	}
	
	public static boolean isGoodPassword(String password, String goodPassword) {
		String encryptedPassword = encryptPassword(password);
		return encryptedPassword.equals(goodPassword);
	}

}
