package com.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.domaine.Comment;
import com.repository.CommentRepository;

@Service
public class CommentCRUD {
	
	@Autowired
	private CommentRepository _commentRepository;
	
	public boolean createComment(String _comment, String _email) {
		_commentRepository.save(new Comment(_comment, _email));
		return true;
	}
	
	public boolean updateComment(long _id, String _comment) {
		if(commentExist(_id)) {
			Comment comment = getCommentId(_id);
			comment.set_comment(_comment);
			_commentRepository.save(comment);
			return true;
		}
		return false;
	}
	
	public boolean deleteComment(long _id) {
		if(commentExist(_id)) {
			_commentRepository.delete(_id);
			return true;
		}
		return false;
	}
	
	public List<Comment> getCommentsByEmail(String _email) { 
		return _commentRepository.findBy_email(_email); 
	} 
	
	public Comment getCommentId(long _id) {
		return _commentRepository.findOne(_id);
	}
	
	private boolean commentExist(long _id) {
		return _commentRepository.exists(_id);
	}
}
