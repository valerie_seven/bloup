package com.service;

import java.util.Timer;
import java.util.TimerTask;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.repository.EventRepository;

import static com.utilitaires.Constante.TIME_EVERY_TEN_MINUTES;
import static com.utilitaires.Constante.TIME_EVERY_HOUR;
import static com.utilitaires.Constante.TIME_DELAY;
import static com.utilitaires.Constante.THREAD_FLAG;

@Component
public class ThreadVerifyTime {

	@Autowired
	private EventRepository _eventRepository;
	
	private Timer timer;
	private TimerTask taskOfThread;
	
	
	public ThreadVerifyTime() {
		super();
		this.timer = new Timer();
		this.taskOfThread = new TimerTask() {
			@Override
			public void run() {
				try {
					calledFunctionByThread();					
				} catch (NullPointerException e) {
				}
			}
		};
		
		if(THREAD_FLAG) {
			//timer.schedule(taskOfThread, TIME_DELAY, 1000*3);
			timer.schedule(taskOfThread, TIME_DELAY, TIME_EVERY_TEN_MINUTES);
		}else {
			timer.schedule(taskOfThread, TIME_DELAY, TIME_EVERY_HOUR);	
		}
	}

	protected void calledFunctionByThread() {
		System.out.println(_eventRepository.findAll());
	}
}
