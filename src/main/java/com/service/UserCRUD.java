package com.service; 
 
import static com.utilitaires.Constante.EVENT_LIST_EMPTY;
import static com.utilitaires.Constante.USER_DOES_EXIST;
import static com.utilitaires.Constante.USER_DOES_NOT_EXIST;
import static com.utilitaires.Constante.REGISTER_SUCCES;
import static com.utilitaires.Constante.ERROR_EMAIL_EXIST;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.domaine.Password;
import com.domaine.User;
import com.repository.PasswordRepository;
import com.repository.UserRepository; 
 
@Service
public class UserCRUD { 
   
	@Autowired 
    private UserRepository _userRepository; 

	@Autowired
    private PasswordRepository _PasswordRepository;
	
	@Autowired
	private TmpUserCrud _tmpUserCRUD;
   
	public String registerUser(String email, String password, String confPassword, String firstName, String lastName) {
		if (this.userExist(email))
			return ERROR_EMAIL_EXIST;
		
		String errorMessage = UserUtil.validateInformation(email, password, confPassword, firstName, lastName);
		if (errorMessage.equals(REGISTER_SUCCES))
			_tmpUserCRUD.createTmpUser(email, firstName, lastName, password);
		return errorMessage;
	}
	
	public void createUser(String email, String password, String firstName, String lastName) { 
		_PasswordRepository.save(new Password(email, PasswordUtil.encryptPassword(password)));
		_userRepository.save(new User(email, firstName, lastName));
    } 
	
	public String updateUserName(String _email, String _firstName, String _lastName) { 
		if(userExist(_email)) {
			User user = getUserByEmail(_email);
			user.setFirstName(_firstName);
			user.setLastName(_lastName);
			_userRepository.save(user);
			return "Votre nom a bien ete change";
		}
		return "Impossible de changer votre nom";
    }
   
	public User getUserByEmail(String email) { 
		List<User> result =  _userRepository.findBy_email(email); 
		if(!result.isEmpty()) 
			return result.get(0); 
		return null; 
	} 
	
	public boolean userExist(String userEmail) {
		if(_userRepository.findBy_email(userEmail).size() != EVENT_LIST_EMPTY)
			return USER_DOES_EXIST;
		return USER_DOES_NOT_EXIST;
	}
	
} 