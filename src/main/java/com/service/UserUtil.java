package com.service;

import static com.utilitaires.Constante.ERROR_EMAIL_INVALID;
import static com.utilitaires.Constante.ERROR_NOM_PRENOM_EMPTY;
import static com.utilitaires.Constante.ERROR_PASSWORDS_DONT_MATCH;
import static com.utilitaires.Constante.ERROR_PASSWORD_INVALID;
import static com.utilitaires.Constante.PATTERN_VERIFY_EMAIL;
import static com.utilitaires.Constante.REGISTER_SUCCES;

import java.util.regex.Pattern;

public class UserUtil {
	
	public static String validateInformation(String email, String password, String confPassword, String prenom,
			String nom) {
		if (!isEmailValid(email))
			return ERROR_EMAIL_INVALID;
		if (!PasswordUtil.isPasswordValid(password))
			return ERROR_PASSWORD_INVALID;
		if (!PasswordUtil.isConfirmPasswordMatch(password, confPassword))
			return ERROR_PASSWORDS_DONT_MATCH;
		if (prenom.isEmpty() || nom.isEmpty())
			return ERROR_NOM_PRENOM_EMPTY;
		return REGISTER_SUCCES;
	}
	
	public static boolean isEmailValid(String email) {
		if (!Pattern.matches(PATTERN_VERIFY_EMAIL, email))
			return false;
		return true;
	}
}
