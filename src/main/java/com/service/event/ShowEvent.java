package com.service.event; 
 
import java.util.ArrayList;
import java.util.Date; 
import java.util.List;

import org.springframework.stereotype.Service; 
 
import com.domaine.Event;
 
@Service 
public class ShowEvent extends ServiceEvent{ 
	
  public List<Event> listEvent = null;
	
  public Event getEventByEventId(Long idEvent) { 
    return _eventRepository.findOne(idEvent); 
  } 
   
  public List<Event> getAllEventsOfUser(String userEmail) { 
    return _eventRepository.findBy_emailUser(userEmail); 
  } 
   
  public List<Event> getAllEvents() { 
	  return _eventRepository.findAll(); 
  } 
  
  public List<Event> getAllEventsByEventTitleAndUserEmail(String eventTitle, String userEmail) { 
	  listEvent = _eventRepository.findBy_eventTitle(eventTitle);
	  try {
		  return getEventsOfUser(userEmail);		  
	  }catch (RuntimeException e) {
		  return clearEventList();
	  }
  } 
   
  public List<Event> getEventsByEventStartingDateAndUserEmail(Date eventStartingDate,
		  String userEmail) { 
	  listEvent = _eventRepository.findBy_eventStartingDate(eventStartingDate);
	  try {
		  return getEventsOfUser(userEmail);		  
	  }catch (RuntimeException e) {
		  return clearEventList();
	  } 
  } 
  
  	private List<Event> getEventsOfUser(String userEmail){
  		if(listEvent == null || listEvent.isEmpty()) {
  	  		throw new RuntimeException();
  		}
  		return getEventsOfUserFromList(userEmail);
  	}
  	
  	private List<Event> getEventsOfUserFromList(String userEmail){
  		List<Event> eventsOfUser = new ArrayList<>();
  		for (Event event : listEvent) {
			if(event.get_email().equalsIgnoreCase(userEmail)) {
				eventsOfUser.add(event);
			}
		}
  		listEvent = null;
		return eventsOfUser;
  	}

  	private List<Event> clearEventList(){
  		listEvent.clear();
  		return listEvent;
  	}
} 