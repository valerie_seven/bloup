package com.service.event;

import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.repository.EventRepository;
import com.service.UserCRUD;

@Service
public class ServiceEvent {

	@Autowired
	protected EventRepository _eventRepository;
	
	@Autowired
	protected UserCRUD validationUser;
	
	public boolean eventExist(Long idEvent) {
		return _eventRepository.exists(idEvent);
	}

	protected boolean userExist(String userEmail) {
		return validationUser.userExist(userEmail);
	}
	
	//TODO Checker si pas d'autre verification nécessaire
	//TODO refactor magic number
	public boolean eventVerification(Date startingDate, Date endingDate, 
			boolean isRepetitive, int repetitionType, String userEmail) {
		if(startingDate == null) {
			return false;
		}
		if(endingDate != null && startingDate.after(endingDate)) {
			return false;
		}
		if(!userExist(userEmail)) {
			return false;			
		}
		if( (isRepetitive && repetitionType == 0) || (!isRepetitive && repetitionType != 0) ) {
			return false;
		}
		if(repetitionType > 7) {
			return false;
		}
		return true;
	}
}
