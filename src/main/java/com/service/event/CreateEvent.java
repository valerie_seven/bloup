package com.service.event;

import java.util.Date;

import org.springframework.stereotype.Service;

import static com.utilitaires.Constante.*;

import com.domaine.Event;

@Service
public class CreateEvent extends ServiceEvent{
	
	public boolean createBasicAllDayEvent(String eventTitle, Date eventStartingDate, String email) {
		if(eventVerification(eventStartingDate, EVENT_NO_ENDING_DATE, 
		  EVENT_IS_NOT_REPEATING, EVENT_NO_REPETITIONS, email)) {
			Event event = new Event(eventTitle, eventStartingDate, EVENT_NO_ENDING_DATE, email);
			_eventRepository.save(event);
			return EVENT_OPERATION_SUCCESSFUL;
		}
		return EVENT_OPERATION_FAILED;
	}
	
	public boolean createBasicEvent(String eventTitle, Date eventStartingDate,
			Date eventEndingDate, String email) {
		if(eventVerification(eventStartingDate, eventEndingDate, 
				  EVENT_IS_NOT_REPEATING, EVENT_NO_REPETITIONS, email)) {
			Event event = new Event(eventTitle, eventStartingDate, eventEndingDate, email);
			_eventRepository.save(event);
			return EVENT_OPERATION_SUCCESSFUL;
		}
		return EVENT_OPERATION_FAILED;
	}

	public boolean createCompleteAllDayEvent(String eventTitle, Date eventStartingDate, 
			boolean isRepetitive, int repititionType, String comments, String email) {
		if(eventVerification(eventStartingDate, EVENT_NO_ENDING_DATE, 
				isRepetitive, repititionType, email)) {
			Event event = new Event(eventTitle, eventStartingDate, EVENT_NO_ENDING_DATE, 
					isRepetitive, repititionType, comments, email);
			_eventRepository.save(event);
			return EVENT_OPERATION_SUCCESSFUL;
		}
		return EVENT_OPERATION_FAILED;
	}
	
	public boolean createCompleteEvent(String eventTitle, Date eventStartingDate, 
			Date eventEndingDate, boolean isRepetitive, int repititionType, 
			String comments, String email) {
		if(eventVerification(eventStartingDate, eventEndingDate, 
				isRepetitive, repititionType, email)) {
			Event event = new Event(eventTitle, eventStartingDate, eventEndingDate, isRepetitive, 
					repititionType, comments, email);
			_eventRepository.save(event);
			return EVENT_OPERATION_SUCCESSFUL;
		}
		return EVENT_OPERATION_FAILED;
	}
}
