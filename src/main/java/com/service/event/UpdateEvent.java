package com.service.event;

import org.springframework.stereotype.Service;

import static com.utilitaires.Constante.*;

import java.util.Date;

import com.domaine.Event;

@Service
public class UpdateEvent extends ServiceEvent{
		
	public boolean saveUpdatedDatesEvent(Long idEvent, Date eventStartingDate,
			Date eventEndingDate, String userEmail) {
		if(eventExist(idEvent)) {
			if(eventVerification(eventStartingDate, eventEndingDate, EVENT_IS_NOT_REPEATING, 
					EVENT_NO_REPETITIONS, userEmail)) {
				return changeAndSaveEventDates(idEvent, eventStartingDate, 
						eventEndingDate, userEmail);
			}
		}
		return EVENT_OPERATION_FAILED;
	}

	public boolean saveUpdatedEvent(Long idEvent, String eventTitle, Date eventStartingDate, 
	  Date eventEndingDate, boolean isRepetitive, int repititionType,
	  String comments, String userEmail) {
		if(eventExist(idEvent)) {
			if(eventVerification(eventStartingDate, eventEndingDate, isRepetitive, 
					repititionType, userEmail)) {
				return changeAndSaveEvent(idEvent, eventTitle, eventStartingDate, 
					eventEndingDate, isRepetitive, repititionType, comments, userEmail);
			}
		}
		return EVENT_OPERATION_FAILED;
	}
	
	private boolean changeAndSaveEventDates(Long idEvent, Date eventStartingDate,
			Date eventDateFin, String email) {
		Event eventToChange = _eventRepository.findOne(idEvent);
		eventToChange.setEventStartingDate(eventStartingDate);
		eventToChange.setEventEndingDate(eventDateFin);
		_eventRepository.save(eventToChange);
		return EVENT_OPERATION_SUCCESSFUL;
	}
	
	private boolean changeAndSaveEvent(Long idEvent, String eventTitle, 
			Date eventStartingDate, Date eventDateFin, boolean isRepetitive,
			int repititionType, String comments, String email) {
		Event eventToChange = _eventRepository.findOne(idEvent);
		eventToChange.set_title(eventTitle);
		eventToChange.setEventStartingDate(eventStartingDate);
		eventToChange.setEventEndingDate(eventDateFin);
		eventToChange.setRepetitive(isRepetitive);
		eventToChange.setRepititionType(repititionType);
		eventToChange.setComments(comments);
		eventToChange.set_email(email);
		_eventRepository.save(eventToChange);
		return EVENT_OPERATION_SUCCESSFUL;
	}
	
	public boolean deleteEvent(Long idEvent) {
		if(eventExist(idEvent)) {
			_eventRepository.delete(idEvent);
			return EVENT_OPERATION_SUCCESSFUL;
		}
		return EVENT_OPERATION_FAILED;
	}
	
	public void deleteAllEvents() {
		_eventRepository.deleteAll();
	}

}