package com.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.domaine.Password;
import com.domaine.TemporaryUser;

public interface TmpUserRepository extends JpaRepository<TemporaryUser, Long> {
	List<TemporaryUser> findBy_tmpCodeId(String _tmpCodeId);
}
