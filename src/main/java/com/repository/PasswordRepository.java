package com.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.domaine.Password;
import com.domaine.User;

public interface PasswordRepository extends JpaRepository<Password, Long> {
	List<Password> findBy_email(String _email);
}
