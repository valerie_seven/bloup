package com.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.domaine.User;

public interface UserRepository extends JpaRepository<User, Long> {
	List<User> findBy_email(String _email);
	List<User> findBy_lastName(String _lastName);
}
