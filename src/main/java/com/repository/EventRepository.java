package com.repository;

import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.domaine.Event;

public interface EventRepository extends JpaRepository<Event, Long> {
	List<Event> findBy_eventStartingDate (Date _eventStartingDate);
	List<Event> findBy_eventTitle (String _eventTitle);
	List<Event> findBy_emailUser(String _emailUser);
	//List<Event> findBy_eventDate(Date _eventDate);
	//List<Event> findBy_userEmail(Long _userId);
}
