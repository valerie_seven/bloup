package com.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.domaine.Comment;

public interface CommentRepository extends JpaRepository<Comment, Long> {
	List<Comment> findBy_email(String _email);
}
