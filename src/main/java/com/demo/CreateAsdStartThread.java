package com.demo;

import java.util.Calendar;
import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.service.ThreadVerifyTime;
import com.service.UserCRUD;
import com.service.event.CreateEvent;

/**
 * A des fins de test uniquement. Ajoute 7 events + 1 utilisateurs dans la DB
 */
@Service
public class CreateAsdStartThread {

	@Autowired
	private CreateEvent eventRepository;
	
	@Autowired
	private UserCRUD userRepository;
	
	public void initializeDbAndCreateThread() {
		
		userRepository.createUser("asd@asd.com", "Asd1", "asd", "ASD");
		
		eventRepository.createBasicEvent("event1", new Date(), null, "asd@asd.com");
		eventRepository.createBasicEvent("Concert", createDate(2017, 9, 28, 20, 0), null, "asd@asd.com");
		eventRepository.createBasicEvent("Dentiste", createDate(2017, 9, 31, 14, 30), null, "asd@asd.com");
		eventRepository.createBasicEvent("Travail", createDate(2017, 9, 24, 15, 0), null, "asd@asd.com");
		eventRepository.createBasicEvent("Devoir", createDate(2017, 9, 30, 16, 45), null, "asd@asd.com");
		eventRepository.createBasicEvent("Fete a Bob", createDate(2017, 9, 10, 0, 0), null, "asd@asd.com");
		eventRepository.createBasicEvent("TestEventTitle", new Date(), null, "asd@asd.com");

		ThreadVerifyTime thread = new ThreadVerifyTime();
	}

	private Date createDate(int year, int month, int day, int hour, int minute) {
		Calendar cal = Calendar.getInstance();
        cal.set(Calendar.YEAR, year);
        cal.set(Calendar.MONTH, month);
        cal.set(Calendar.DAY_OF_MONTH, day);
        cal.set(Calendar.HOUR_OF_DAY, hour);
        cal.set(Calendar.MINUTE, minute);
        Date date = cal.getTime();
		return date;
	}
	
}
