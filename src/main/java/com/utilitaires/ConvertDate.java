package com.utilitaires;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;

import static com.utilitaires.Constante.EVENT_DATE_ALLDAY_FORMAT;
import static com.utilitaires.Constante.EVENT_DATE_LOCALTIME_FORMAT;


public class ConvertDate {
	/**
	 * Suit la notation yyyy-MM-dd ou yyyy-MM-ddTHH:mm.
	 * @return Une Date ou Null si incapable de convertir en String.
	 */
	public static Date stringToDate(String dateInString) {
		try {
			if(dateInString.length() == 0) {
				return null;
			}
			if(dateInString.length() > 11) {
				return formatDate(dateInString, EVENT_DATE_LOCALTIME_FORMAT);
	    	}
	    	else {
	    		return formatDate(dateInString, EVENT_DATE_ALLDAY_FORMAT);
	    	}
		} catch (ParseException pe) {
			try {
				return formatDate(dateInString, EVENT_DATE_ALLDAY_FORMAT);
			}catch (Exception e) {
				return null;
			}
		} 
    }

	private static Date formatDate(String dateInString, String dateFormat) throws ParseException {
		SimpleDateFormat format = new SimpleDateFormat(dateFormat);
		format.setTimeZone(TimeZone.getTimeZone("UTC"));
		return format.parse(dateInString);
	}
}
