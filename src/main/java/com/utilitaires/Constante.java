package com.utilitaires;

import java.util.Date;

public class Constante {
	public static final boolean THREAD_FLAG = true;
	
	public static final String URL_CREATE_BASIC_ALLDAY_EVENT = "/Event/createBAE";
	public static final String URL_CREATE_COMPLEX_ALLDAY_EVENT = "/Event/createCAE";
	public static final String URL_CREATE_BASIC_EVENT = "/Event/createBE";
	public static final String URL_CREATE_COMPLEX_EVENT = "/Event/createCE";

	public static final String URL_FIND_BY_EVENT_ID = "/Event/getById";
	public static final String URL_FIND_BY_USER_EMAIl = "/Event/getByUser";
	public static final String URL_FIND_ALL = "/Event/getAll";
	public static final String URL_FIND_BY_TITLE = "/Event/getByTitle";
	public static final String URL_FIND_BY_EVENT_DATE = "/Event/getByDate";

	public static final String URL_UPDATE_DATE_EVENT = "/Event/sUpdateEvent";
	public static final String URL_UPDATE_EVENT = "/Event/updateEvent";
	public static final String URL_DELETE_EVENT = "/Event/deleteEvent";
	
	public static final String URL_GET_FACT_DIDYOUKNOW = "/Fact/Didyouknow";
	
	public static final String PARAMS_EVENT_ID = "eventId";
	public static final String PARAMS_TITLE = "title";
	public static final String PARAMS_STARTING_DATE = "eventStartingDate";
	public static final String PARAMS_ENDING_DATE = "eventEndingDate";
	public static final String PARAMS_IS_REPETITIVE = "isRepetitive";
	public static final String PARAMS_REPETITION_TYPE = "repetitionType";
	public static final String PARAMS_COMMENT = "comment";
	public static final String PARAMS_USER_EMAIL = "userEmail";
	
	public static final boolean EVENT_OPERATION_SUCCESSFUL = true;
	public static final boolean EVENT_OPERATION_FAILED = false;

	public static final String EVENT_DATE_FORMAT = "yyyy-MM-dd-HH";
	public static final String EVENT_DATE_ALLDAY_FORMAT = "yyyy-MM-dd";
	public static final String EVENT_DATE_LOCALTIME_FORMAT = "yyyy-MM-dd'T'HH:mm";
	
	public static final boolean EVENT_IS_REPEATING = true;
	public static final boolean EVENT_IS_NOT_REPEATING = false;
	public static final boolean USER_DOES_EXIST = true;
	public static final boolean USER_DOES_NOT_EXIST = false;
	public static final Date EVENT_NO_ENDING_DATE = null;
	public static final int EVENT_NO_REPETITIONS = 0;
	public static final int EVENT_LIST_EMPTY = 0;
	public static final String EVENT_NO_COMMENTS = "";
	public static final String EVENT_NO_TITLE = "";
	
	public static final String REGISTER_SUCCES = "";
	public static final String ERROR_EMAIL_EXIST = "Cette adresse email est deja utilise";
	public static final String ERROR_EMAIL_INVALID = "Cette adresse email est invalide.";
	public static final String ERROR_PASSWORDS_DONT_MATCH = "Le mot de passe est different du mot de passe de confirmation.";
	public static final String ERROR_PASSWORD_INVALID = "Les mot de passe est invalide : il doit contenir une majuscule et un chiffre.";
	public static final String ERROR_NOM_PRENOM_EMPTY = "Le nom et le prenom ne peuvent pas etre null";
	public static final String ERROR_WRONG_PASSWORD = "Le mot de passe est invalide";
	
	public static final String PATTERN_VERIFY_PASSWORD = "(?=.*\\d)(?=.*[A-Z])";
	public static final String PATTERN_VERIFY_EMAIL = "(?:[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\\.[a-z0-9!#$%&'*+/=?^_`"
			+ "{|}~-]+)*|\"(?:[\\x01-\\x08\\x0b\\x0c\\x0e-\\x1f\\x21\\x23-\\x5b\\x5d-\\x7f]|\\\\[\\x01-\\x09\\x0b\\"
			+ "x0c\\x0e-\\x7f])*\")@(?:(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?|\\[(?"
			+ ":(?:(2(5[0-5]|[0-4][0-9])|1[0-9][0-9]|[1-9]?[0-9]))\\.){3}(?:(2(5[0-5]|[0-4][0-9])|1[0-9][0-9]|[1-9]?"
			+ "[0-9])|[a-z0-9-]*[a-z0-9]:(?:[\\x01-\\x08\\x0b\\x0c\\x0e-\\x1f\\x21-\\x5a\\x53-\\x7f]|\\\\[\\x01-\\x09"
			+ "\\x0b\\x0c\\x0e-\\x7f])+)\\])";
	
	public static final long TIME_EVERY_TEN_MINUTES = 1000*60*10;
	public static final long TIME_EVERY_HOUR = 1000*60*60;
	public static final long TIME_DELAY = 01;
	
	public static final String HARDCODED_FACT = "La priorite a droite existe en avion.";
	public static final String PATH_DID_YOU_KNOW_FILE = "ListeSavoirInutile.txt";

}
