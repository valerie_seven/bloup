package com.test.service;

import static org.junit.Assert.*;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import com.service.LoginUtil;
import com.service.PasswordCRUD;
import com.service.PasswordUtil;
import com.service.UserCRUD;

import static com.utilitaires.Constante.REGISTER_SUCCES;

@RunWith(MockitoJUnitRunner.class)
public class TestLoginRegisterUtil {

	private String goodPassword = PasswordUtil.encryptPassword("GoodPassword");
	
	@Mock
	public UserCRUD userCrud;

	@Mock
	public UserCRUD serviceUser;
	
	@Mock
	public PasswordCRUD passwordCrud;
	
	@InjectMocks
	public LoginUtil loginUtil;
	
	@Test
	public void testVerifyLoginPass() {
		Mockito.when(serviceUser.userExist("EmailExist")).thenReturn(true);
		Mockito.when(passwordCrud.getPasswordByEmail("EmailExist").get_password()).thenReturn(goodPassword);
		assertEquals(REGISTER_SUCCES, 
				loginUtil.verifyLogin("EmailExist", "GoodPassword"));
	}	
	
//	@Test
//	public void testVerifyLoginFailBadEmail() {
//		Mockito.when(serviceUser.userExist("EmailDontExist")).thenReturn(false);
//		assertEquals(loginUtil._ERROR_WRONG_PASSWORD, 
//				loginUtil.verifyLogin("EmailDontExist", "GoodPassword"));
//	}
//	
//	@Test
//	public void testVerifyLoginFailBadPassword() {
//		Mockito.when(serviceUser.userExist("EmailExist")).thenReturn(true);
//		Mockito.when(passwordCrud.getPasswordByEmail("EmailExist")).thenReturn(goodPassword);
//		assertEquals(loginUtil._ERROR_WRONG_PASSWORD, 
//				loginUtil.verifyLogin("EmailExist", "BadPassword"));
//	}
//	
//	//TODO mettre quequchose
//	@Test
//	public void testLoadUser() {
//		
//	}
//
//	@Test
//	public void testCheckPasswordIsValidPass() {
//		assertTrue(loginUtil.CheckPasswordIsValid("GoodPassword1"));
//	}
//
//	@Test
//	public void testCheckPasswordIsValidFailMissingNumber() {
//		assertFalse(loginUtil.CheckPasswordIsValid("BadPassword"));
//	}
//
//	@Test
//	public void testCheckPasswordIsValidFailMissingCapitalLetter() {
//		assertFalse(loginUtil.CheckPasswordIsValid("badpassword1"));
//	}
//
//	@Test
//	public void testCheckEmailIsValidPass() {
//		assertTrue(loginUtil.CheckEmailIsValid("test@test.com"));
//	}
//
//	@Test
//	public void testCheckEmailIsValidFailNoDot() {
//		assertFalse(loginUtil.CheckEmailIsValid("test@test"));
//	}
//
//	@Test
//	public void testCheckEmailIsValidFailNothingAfterDot() {
//		assertFalse(loginUtil.CheckEmailIsValid("test@test."));
//	}
//
//	@Test
//	public void testCheckEmailIsValidFailNothingBetweenDotAndA() {
//		assertFalse(loginUtil.CheckEmailIsValid("test@.com"));
//	}
//
//	@Test
//	public void testCheckEmailIsValidFailNothingBeforeA() {
//		assertFalse(loginUtil.CheckEmailIsValid("@test.ca"));
//	}
//
//	@Test
//	public void testCheckEmailIsValidFailMissingA() {
//		assertFalse(loginUtil.CheckEmailIsValid("testtest.ca"));
//	}
//
//	@Test
//	public void testCheckEmailIsValidFailContainCapital() {
//		assertFalse(loginUtil.CheckEmailIsValid("Test@test.ca"));
//	}
//	
//	@Test
//	public void testValidateRegisterInformationPass() {
//		assertEquals(loginUtil._REGISTER_SUCCES, loginUtil
//				.validateRegisterInformation("email@valid.com","GoodPassword1", 
//				"GoodPassword1", "Bob", "Bob"));
//	}
//	
//	@Test
//	public void testValidateRegisterInformationFailEmailAlreadyExist() {
//		Mockito.when(serviceUser.userExist("Email@Exist.com")).thenReturn(true);
//		assertEquals(loginUtil._ERROR_EMAIL_EXIST, loginUtil
//				.validateRegisterInformation("Email@Exist.com","GoodPassword1", 
//				"GoodPassword1", "Bob", "Bob"));
//	}
//	
//	@Test
//	public void testValidateRegisterInformationFailEmailNotValid() {
//		assertEquals(loginUtil._ERROR_EMAIL_INVALID, loginUtil
//				.validateRegisterInformation("EmailNotValid","GoodPassword1", 
//				"GoodPassword1", "Bob", "Bob"));
//	}
//	
//	@Test
//	public void testValidateRegisterInformationFailPasswordNotValid() {
//		assertEquals(loginUtil._ERROR_PASSWORD_INVALID, loginUtil
//				.validateRegisterInformation("email@valid.com", "badpassword", 
//				"badpassword", "Bob", "Bob"));
//	}
//	
//	@Test
//	public void testValidateRegisterInformationFailPasswordsDontMatch() {
//		assertEquals(loginUtil._ERROR_PASSWORDS_DONT_MATCH, loginUtil
//				.validateRegisterInformation("email@valid.com", "GoodPassword1", 
//				"GoodPassword2", "Bob", "Bob"));
//	}
//	
//	@Test
//	public void testValidateRegisterInformationFailFirstNameEmpty() {
//		assertEquals(loginUtil._ERROR_NOM_PRENOM_EMPTY, loginUtil
//				.validateRegisterInformation("email@valid.com", "GoodPassword1", 
//				"GoodPassword1", "", "Bob"));
//	}
//	
//	@Test
//	public void testValidateRegisterInformationFailLastNameEmpty() {
//		assertEquals(loginUtil._ERROR_NOM_PRENOM_EMPTY, loginUtil
//				.validateRegisterInformation("email@valid.com", "GoodPassword1", 
//				"GoodPassword1", "Bob", ""));
//	}
//	
//	@Test
//	public void testRegisterUserPass() {
//		assertEquals(loginUtil._REGISTER_SUCCES, loginUtil
//				.registerUser("email@valid.com", "GoodPassword1", 
//				"GoodPassword1", "Bob", "Bob"));
//		Mockito.verify(userCrud, times(1)).createUser(anyString(),
//				anyString(), anyString(), anyString());
//	}
//	
//	@Test
//	public void testRegisterUserFail() {
//		assertEquals(loginUtil._ERROR_EMAIL_INVALID, loginUtil
//				.registerUser("EmailNotValid", "GoodPassword1", 
//				"GoodPassword1", "Bob", "Bob"));
//		Mockito.verify(userCrud, never()).createUser(anyString(),
//				anyString(), anyString(), anyString());
//	}
//	

}
