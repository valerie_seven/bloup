package com.test.service.event;

import static org.junit.Assert.*;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyLong;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import com.domaine.Event;
import com.repository.EventRepository;
import com.service.UserCRUD;
import com.service.event.ShowEvent;

@SuppressWarnings("deprecation")
@RunWith(MockitoJUnitRunner.class)
public class TestShowEvent {

	private Event eventTest;
	private List<Event> eventListTest;
	private List<Event> _allEvents;
	private List<Event> _eventsSameTitle;
	private List<Event> _eventsSameEmail;
	private Event _event1;	
	private Event _event2;
	private Date _dateEvent2;
	
	@Mock
	public EventRepository eventRepo;
	
	@Mock
	public UserCRUD serviceUser;
	
	@InjectMocks
	public ShowEvent showEvent;
	
	@Before
	public void setUpServiceUserAndEventRepo() {
		eventTest = new Event();
		eventTest.set_email("TestEventExist");
		eventListTest = new ArrayList<>();
		fillTheRepoWithEvent();
	}
	
	@Test
	public void testGetEventByEventIdPass() {
		Mockito.when(eventRepo.findOne(1l)).thenReturn(eventTest);
		assertEquals("TestEventExist", showEvent.getEventByEventId(1l).get_email());
		Mockito.verify(eventRepo, times(1)).findOne(anyLong());
	}
	
	@Test
	public void testGetEventByEventIdFail() {
		Mockito.when(eventRepo.findOne(2l)).thenReturn(null);
		assertNull(showEvent.getEventByEventId(2l));
		Mockito.verify(eventRepo, times(1)).findOne(anyLong());
	}
	
	@Test
	public void testGetEventByEventId() {
		when(eventRepo.findOne(1L)).thenReturn(_event1);
		assertEquals(_event1, showEvent.getEventByEventId(1L));
	}
	
	@Test
	public void testGetAllEventsOfUserPass() {
		eventListTest.add(new Event());
		Mockito.when(eventRepo.findBy_emailUser(anyString())).thenReturn(eventListTest);
		assertFalse(showEvent.getAllEventsOfUser("TestEventExist").isEmpty());
		Mockito.verify(eventRepo, times(1)).findBy_emailUser(anyString());
	}
	
	@Test
	public void testGetAllEventsOfUserFail() {
		Mockito.when(eventRepo.findBy_emailUser(anyString())).thenReturn(eventListTest);
		assertTrue(showEvent.getAllEventsOfUser("TestEventDontExist").isEmpty());
		Mockito.verify(eventRepo, times(1)).findBy_emailUser(anyString());
	}
	
	@Test
	public void testgetAllEventsOfUser() {
		when(eventRepo.findBy_emailUser("asd@asd")).thenReturn(_eventsSameEmail);
		assertEquals(_eventsSameEmail, showEvent.getAllEventsOfUser("asd@asd"));
	}
	
	@Test
	public void testGetAllEventsPass() {
		Mockito.when(eventRepo.findAll()).thenReturn(eventListTest);
		assertTrue(showEvent.getAllEvents().isEmpty());
		Mockito.verify(eventRepo, times(1)).findAll();
	}
	
	@Test
	public void testGetAllEvents() {
		when(eventRepo.findAll()).thenReturn(_allEvents);
		assertEquals(_allEvents, showEvent.getAllEvents());
	}
	
	@Test
	public void testGetAllEventsByEventTitleAndUserEmailPass() {
		eventListTest.add(eventTest);
		Mockito.when(eventRepo.findBy_eventTitle(anyString())).thenReturn(eventListTest);
		assertFalse(showEvent.
			getAllEventsByEventTitleAndUserEmail("GoodTitle", "TestEventExist").isEmpty());
		Mockito.verify(eventRepo, times(1)).findBy_eventTitle(anyString());
	}
	
	@Test
	public void testGetAllEventsByEventTitleAndUserEmailListEmpty() {
		Mockito.when(eventRepo.findBy_eventTitle(anyString())).thenReturn(eventListTest);
		assertTrue(showEvent.
			getAllEventsByEventTitleAndUserEmail("GoodTitle", "TestEventExist").isEmpty());
		Mockito.verify(eventRepo, times(1)).findBy_eventTitle(anyString());
	}
	
	@Test
	public void testGetAllEventsByEventTitleAndUserEmail() {
		when(eventRepo.findBy_eventTitle("TestEventTitle")).thenReturn(_eventsSameTitle);
		List<Event> eventsSameTitleAndEmail = new ArrayList<>();
		eventsSameTitleAndEmail.add(_eventsSameTitle.get(0));
		eventsSameTitleAndEmail.add(_eventsSameTitle.get(1));
		
		assertEquals(eventsSameTitleAndEmail, showEvent.getAllEventsByEventTitleAndUserEmail
				("TestEventTitle", "asd@asda"));
	}
	
	@Test
	public void testGetEventsByEventStartingDateAndUserEmail() {
		eventListTest.add(eventTest);
		Mockito.when(eventRepo.findBy_eventStartingDate(any(Date.class))).thenReturn(eventListTest);
		assertFalse(showEvent.
				getEventsByEventStartingDateAndUserEmail(new Date(), "TestEventExist").isEmpty());
		Mockito.verify(eventRepo, times(1)).findBy_eventStartingDate(any(Date.class));
	}

	@Test
	public void testGetEventsByEventDateAndUserEmail() {
		List<Event> list = new ArrayList<>();
		list.add(_event2);
		when(eventRepo.findBy_eventStartingDate(_dateEvent2)).thenReturn(list);

		assertEquals(_event2, showEvent
			.getEventsByEventStartingDateAndUserEmail(new Date(2000, 01, 01), "asd@asd").get(0));
	}
	
	private void fillTheRepoWithEvent() {
		initializeArray();
		
		_event1 = new Event("event1", new Date(), null, "asd@asd");
		_event1.setEventId(1L);
		saveEventSameEmail(_event1);

		_dateEvent2 = new Date(2000, 01, 01);
		_event2 = new Event("event2", _dateEvent2, null, "asd@asd");
		saveEventSameEmail(_event2);

		Event event = new Event("event3", new Date(), null, "asd@asda");
		event.set_title("TestEventTitle");
		saveEventSameTitle(event);
		
		event = new Event("event4", new Date(), null, "asd@asda");
		event.set_title("TestEventTitle");
		saveEventSameTitle(event);
		
		event = new Event("event5", new Date(), null, "a@a");
		event.set_title("TestEventTitle");
		saveEventSameTitle(event);	
	}
	private void initializeArray() {
		_allEvents = new ArrayList<>();
		_eventsSameTitle = new ArrayList<>();
		_eventsSameEmail = new ArrayList<>();
	}
	private void saveEventSameEmail(Event event) {
		eventRepo.save(event);
		_allEvents.add(event);
		_eventsSameEmail.add(event);
	}
	private void saveEventSameTitle(Event event) {
		eventRepo.save(event);
		_allEvents.add(event);
		_eventsSameTitle.add(event);
	}
}
