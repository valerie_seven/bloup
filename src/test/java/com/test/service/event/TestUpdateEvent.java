package com.test.service.event;

import static org.junit.Assert.*;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import com.domaine.Event;
import com.repository.EventRepository;
import com.service.UserCRUD;
import com.service.event.UpdateEvent;

@RunWith(MockitoJUnitRunner.class)
public class TestUpdateEvent {

	@Mock
	public EventRepository eventRepo;
	
	@Mock
	public UserCRUD serviceUser;
	
	@InjectMocks
	public UpdateEvent updateEvent;
	
	@Before
	public void setUpServiceUserAndEventRepo() {
		Mockito.when(serviceUser.userExist("emailExist")).thenReturn(true);
		Mockito.when(eventRepo.findOne(1l)).thenReturn(new Event());
		Mockito.when(eventRepo.exists(1l)).thenReturn(true);
		Mockito.when(eventRepo.exists(2l)).thenReturn(false);
	}
	
	@Test
	public void testSaveUpdatedDatesEventPass() throws ParseException {
		Date startDate = new SimpleDateFormat("yyyyMMdd").parse("20100520");
		Date endDate = new SimpleDateFormat("yyyyMMdd").parse("20100521");
		assertTrue(updateEvent.saveUpdatedDatesEvent(1l, startDate, endDate, "emailExist"));
	}
	
	@Test
	public void testSaveUpdatedDatesEventFail() throws ParseException {
		Date startDate = null;
		Date endDate = new SimpleDateFormat("yyyyMMdd").parse("20100521");
		assertFalse(updateEvent.saveUpdatedDatesEvent(1l, startDate, endDate, "emailExist"));
	}
	
	@Test
	public void testSaveUpdatedEventPass() throws ParseException {
		Date startDate = new SimpleDateFormat("yyyyMMdd").parse("20100520");
		Date endDate = new SimpleDateFormat("yyyyMMdd").parse("20100521");
		assertTrue(updateEvent.
			saveUpdatedEvent(1l ,"Test", startDate, endDate, false, 0, "", "emailExist"));
	}
	
	@Test
	public void testSaveUpdatedEventFail() throws ParseException {
		Date startDate = null;
		Date endDate = new SimpleDateFormat("yyyyMMdd").parse("20100521");
		assertFalse(updateEvent.
			saveUpdatedEvent(1l ,"Test", startDate, endDate, false, 0, "", "emailExist"));
	}
	
	@Test
	public void testDeleteEventPass() throws ParseException {
		assertTrue(updateEvent.deleteEvent(1l));
	}
	
	@Test
	public void testDeleteEventFail() throws ParseException {
		assertFalse(updateEvent.deleteEvent(2l));
	}

}
