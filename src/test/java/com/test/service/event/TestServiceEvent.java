package com.test.service.event;

import static org.junit.Assert.*;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import com.repository.EventRepository;
import com.service.UserCRUD;
import com.service.event.ServiceEvent;

@RunWith(MockitoJUnitRunner.class)
public class TestServiceEvent {

	@Mock
	public EventRepository eventRepo;
	
	@Mock
	public UserCRUD serviceUser;
	
	@InjectMocks
	public ServiceEvent serviceEvent;
	
	@Before
	public void setUpServiceUser() {
		Mockito.when(serviceUser.userExist("emailExist")).thenReturn(true);
		Mockito.when(serviceUser.userExist("emailDontExist")).thenReturn(false);
	}
	
	@Test
	public void testEventExist() {
		Mockito.when(eventRepo.exists(1l)).thenReturn(true);
		assertTrue(serviceEvent.eventExist(1l));		
	}
	
	@Test
	public void testEventDontExist() {
		Mockito.when(eventRepo.exists(1l)).thenReturn(false);
		assertFalse(serviceEvent.eventExist(1l));	
	}
	
	@Test
	public void testEventVerificationPassEndingDateAfterSartingDate() throws ParseException {
		Date startDate = new SimpleDateFormat("yyyyMMdd").parse("20100520");
		Date endDate = new SimpleDateFormat("yyyyMMdd").parse("20100521");
		assertTrue(serviceEvent.eventVerification(startDate, endDate, false, 0, "emailExist"));
	}
	
	@Test
	public void testEventVerificationPassEndingDateNull() throws ParseException {
		Date startDate = new SimpleDateFormat("yyyyMMdd").parse("20100520");
		Date endDate = null;
		assertTrue(serviceEvent.eventVerification(startDate, endDate, false, 0, "emailExist"));
	}
	
	@Test
	public void testEventVerificationFailStartingDateNull() throws ParseException {
		Date startDate = null;
		Date endDate = new SimpleDateFormat("yyyyMMdd").parse("20100521");
		assertFalse(serviceEvent.eventVerification(startDate, endDate, false, 0, "emailExist"));
	}
	
	@Test
	public void testEventVerificationFailEndingDateBeforeStartingDate() throws ParseException {
		Date startDate = new SimpleDateFormat("yyyyMMdd").parse("20100520");
		Date endDate = new SimpleDateFormat("yyyyMMdd").parse("20100519");
		assertFalse(serviceEvent.eventVerification(startDate, endDate, false, 0, "emailExist"));
	}
	
	@Test
	public void testEventVerificationFailEmailDontExist() throws ParseException {
		Date startDate = new SimpleDateFormat("yyyyMMdd").parse("20100520");
		Date endDate = new SimpleDateFormat("yyyyMMdd").parse("20100519");
		assertFalse(serviceEvent.eventVerification(startDate, endDate, false, 0, "emailDontExist"));
	}
	
	@Test
	public void testEventVerificationFailIsRepititiveWhenShouldNot() throws ParseException {
		Date startDate = new SimpleDateFormat("yyyyMMdd").parse("20100520");
		Date endDate = new SimpleDateFormat("yyyyMMdd").parse("20100521");
		assertFalse(serviceEvent.eventVerification(startDate, endDate, true, 0, "emailExist"));
	}
	
	@Test
	public void testEventVerificationFailNotRepititiveWhenShould() throws ParseException {
		Date startDate = new SimpleDateFormat("yyyyMMdd").parse("20100520");
		Date endDate = new SimpleDateFormat("yyyyMMdd").parse("20100521");
		assertFalse(serviceEvent.eventVerification(startDate, endDate, false, 3, "emailExist"));
	}
	
	@Test
	public void testEventVerificationFailRepititionTypeDontExist() throws ParseException {
		Date startDate = new SimpleDateFormat("yyyyMMdd").parse("20100520");
		Date endDate = new SimpleDateFormat("yyyyMMdd").parse("20100521");
		assertFalse(serviceEvent.eventVerification(startDate, endDate, false, 9, "emailExist"));
	}
}
