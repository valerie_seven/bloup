package com.test.service.event;

import static org.junit.Assert.*;
import static org.mockito.Matchers.isA;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import com.repository.EventRepository;
import com.service.UserCRUD;
import com.service.event.CreateEvent;

@RunWith(MockitoJUnitRunner.class)
public class TestCreateEvent {

	@Mock
	public EventRepository eventRepo;
	
	@Mock
	public UserCRUD serviceUser;
	
	@InjectMocks
	public CreateEvent createEvent;
	
	@Before
	public void setUpServiceUserAndEventRepo() {
		Mockito.when(serviceUser.userExist("emailExist")).thenReturn(true);
	}

	@Test
	public void testCreateBasicAlldayEventPass() throws ParseException {
		Date startDate = new SimpleDateFormat("yyyyMMdd").parse("20100520");
		assertTrue(createEvent.createBasicAllDayEvent("Test", startDate, "emailExist"));
		Mockito.verify(eventRepo, times(1)).save(isA(com.domaine.Event.class));
	}

	@Test
	public void testCreateBasicAlldayEventFail() throws ParseException {
		Date startDate = null;
		assertFalse(createEvent.createBasicAllDayEvent("Test", startDate, "emailExist"));
		Mockito.verify(eventRepo, never()).save(isA(com.domaine.Event.class));	
	}

	@Test
	public void testCreateBasicEventPass() throws ParseException {
		Date startDate = new SimpleDateFormat("yyyyMMdd").parse("20100520");
		Date endDate = new SimpleDateFormat("yyyyMMdd").parse("20100521");
		assertTrue(createEvent.createBasicEvent("Test", startDate, endDate, "emailExist"));	
		Mockito.verify(eventRepo, times(1)).save(isA(com.domaine.Event.class));	
	}

	@Test
	public void testCreateBasicEventFail() throws ParseException {
		Date startDate = null;
		Date endDate = new SimpleDateFormat("yyyyMMdd").parse("20100521");
		assertFalse(createEvent.createBasicEvent("Test", startDate, endDate, "emailExist"));
		Mockito.verify(eventRepo, never()).save(isA(com.domaine.Event.class));	
	}

	@Test
	public void testCreateCompleteAllDayEventPass() throws ParseException {
		Date startDate = new SimpleDateFormat("yyyyMMdd").parse("20100520");
		assertTrue(createEvent.
			createCompleteAllDayEvent("Test", startDate, false, 0, "", "emailExist"));		
		Mockito.verify(eventRepo, times(1)).save(isA(com.domaine.Event.class));
	}

	@Test
	public void testCreateCompleteAllDayEventFail() throws ParseException {
		Date startDate = null;
		assertFalse(createEvent.
			createCompleteAllDayEvent("Test", startDate, false, 0, "", "emailExist"));
		Mockito.verify(eventRepo, never()).save(isA(com.domaine.Event.class));	
	}

	@Test
	public void testCreateCompleteEventPass() throws ParseException {
		Date startDate = new SimpleDateFormat("yyyyMMdd").parse("20100520");
		Date endDate = new SimpleDateFormat("yyyyMMdd").parse("20100521");
		assertTrue(createEvent.
			createCompleteEvent("Test", startDate, endDate, false, 0, "", "emailExist"));
		Mockito.verify(eventRepo, times(1)).save(isA(com.domaine.Event.class));		
	}

	@Test
	public void testCreateCompleteEventFail() throws ParseException {
		Date startDate = null;
		Date endDate = new SimpleDateFormat("yyyyMMdd").parse("20100521");
		assertFalse(createEvent.
			createCompleteEvent("Test", startDate, endDate, false, 0, "", "emailExist"));
		Mockito.verify(eventRepo, never()).save(isA(com.domaine.Event.class));		
	}
}
