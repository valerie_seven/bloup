package com.test.service;

import static org.junit.Assert.*;

import org.junit.Test;

import com.service.PasswordUtil;

public class TestPasswordUtil {
	
	@Test
	public void testEncryptPassword() {
		String newPass = PasswordUtil.encryptPassword("password");
		assertFalse(newPass.equals("password"));
		assertTrue(PasswordUtil.encryptPassword("password").equals(newPass));
	}

}
