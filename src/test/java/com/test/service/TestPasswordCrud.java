package com.test.service;

import static org.junit.Assert.*;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import com.domaine.Password;
import com.repository.PasswordRepository;
import com.service.PasswordCRUD;

@RunWith(MockitoJUnitRunner.class)
public class TestPasswordCrud {

	private Password passwordTest;
	private List<Password> passwordListTest;
	
	@Mock
	public PasswordRepository passwordRepo;
	
	@InjectMocks
	public PasswordCRUD passwordCRUD;
	
	
	@Before
	public void setup() {
		passwordTest = new Password();
		passwordTest.set_password("testPassword");
		passwordListTest = new ArrayList<>();
	}
	
	@Test
	public void testGetPasswordByEmailPass() {
		passwordListTest.add(passwordTest);
		Mockito.when(passwordRepo.findBy_email("EmailExist")).thenReturn(passwordListTest);
		assertEquals("testPassword", passwordCRUD.getPasswordByEmail("EmailExist"));
		verify(passwordRepo, times(1)).findBy_email(anyString());
	}
	
	@Test
	public void testGetPasswordByEmailNull() {
		Mockito.when(passwordRepo.findBy_email("EmailDontExist")).thenReturn(passwordListTest);
		assertNull(passwordCRUD.getPasswordByEmail("EmailDontExist"));
		verify(passwordRepo, times(1)).findBy_email(anyString());
	}

}
