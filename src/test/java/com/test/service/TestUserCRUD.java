package com.test.service;

import static org.junit.Assert.*;
import static org.mockito.Matchers.anyString;
import static org.mockito.Matchers.isA;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import com.domaine.Password;
import com.domaine.User;
import com.repository.PasswordRepository;
import com.repository.UserRepository;
import com.service.UserCRUD;

@RunWith(MockitoJUnitRunner.class)
public class TestUserCRUD {

	private User userTest;
	private List<User> userListTest;
	
	@Mock
	public UserRepository userRepository;
	
	@Mock
	public PasswordRepository passwordRepository;
	
	@InjectMocks
	public UserCRUD userCRUD;
	
	@Before
	public void setup() {
		userTest = new User();
		userTest.setEmail("EmailExist");
		userTest.setFirstName("GoodName");
		userListTest = new ArrayList<>();
	}
	
	@Test
	public void testCreateUser() {
		userCRUD.createUser("EmailExist", "Password", "Test", "LastTest");
		Mockito.verify(userRepository, times(1)).save(isA(User.class));
		Mockito.verify(passwordRepository, times(1)).save(isA(Password.class));
	}
	
	@Test
	public void testGetUserByEmailPass() {
		userListTest.add(userTest);
		Mockito.when(userRepository.findBy_email("EmailExist")).thenReturn(userListTest);
		assertEquals("GoodName", userCRUD.getUserByEmail("EmailExist").getFirstName());
		verify(userRepository, times(1)).findBy_email(anyString());
	}
	
	@Test
	public void testGetUserByEmailNull() {
		Mockito.when(userRepository.findBy_email("EmailDontExist")).thenReturn(userListTest);
		assertNull(userCRUD.getUserByEmail("EmailDontExist"));
		verify(userRepository, times(1)).findBy_email(anyString());
	}

}
