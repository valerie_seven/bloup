package com.test.service.user;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.runners.MockitoJUnitRunner;

import com.domaine.User;
import com.repository.UserRepository;
import com.service.UserCRUD;

@RunWith(MockitoJUnitRunner.class)
public class TestServiceUser {

	@Mock
	public UserRepository userRepo;
	
	@InjectMocks
	public UserCRUD serviceUser;
	
	@Before
	public void before() {
		//MockitoAnnotations.initMocks(this);
	}
	
	@Test
	public void testUserExist() {
		List<User> listWithStuff = new ArrayList<>();
		listWithStuff.add(new User());
		Mockito.when(userRepo.findBy_email("emailExist")).thenReturn(listWithStuff);
		assertTrue(serviceUser.userExist("emailExist"));		
	}
	
	@Test
	public void testUserDontExist() {
		List<User> listEmpty = new ArrayList<>();
		Mockito.when(userRepo.findBy_email("emailDontExist")).thenReturn(listEmpty);
		assertFalse(serviceUser.userExist("emailDontExist"));		
	}

}
