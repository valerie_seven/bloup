package com.test.utilitaires;

import static com.utilitaires.Constante.EVENT_DATE_ALLDAY_FORMAT;
import static com.utilitaires.Constante.EVENT_DATE_LOCALTIME_FORMAT;
import static org.junit.Assert.*;

import java.text.ParseException;
import java.text.SimpleDateFormat;

import org.junit.Test;

import com.utilitaires.ConvertDate;


public class TestConvertDate {

	public ConvertDate convertDate = new ConvertDate();
		
	@Test
	public void testStringToDateStringVide() {
		assertNull(convertDate.stringToDate(""));
	}

	@Test
	public void testStringToDateDateFormat() throws ParseException {
		String dateFormat = "2000-01-30";
		assertEquals(new SimpleDateFormat(EVENT_DATE_ALLDAY_FORMAT).parse(dateFormat), convertDate.stringToDate(dateFormat));
	}

	@Test
	public void testStringToDateLocalDateFormat() throws ParseException {
		String dateFormat = "2000-01-30T10:10";
		assertEquals(new SimpleDateFormat(EVENT_DATE_LOCALTIME_FORMAT).parse(dateFormat), convertDate.stringToDate(dateFormat));
	}

	@Test
	public void testStringToDateLocalDateFormatWithProblem() throws ParseException {
		String dateFormat = "2000-01-30-ADSW0000";
		String dateFormatExpected = "2000-01-30";
		assertEquals(new SimpleDateFormat(EVENT_DATE_ALLDAY_FORMAT).parse(dateFormatExpected),
				convertDate.stringToDate(dateFormat));
	}

	@Test
	public void testStringToDateNonDateFormat() throws ParseException {
		String dateFormat = "ImJustABigCancer!HelpMe!";
		assertNull(convertDate.stringToDate(dateFormat));
	}
}
