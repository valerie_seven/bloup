package com.exemple;

import static org.junit.Assert.*;

import java.util.Date;

import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import com.domaine.Event;
import com.domaine.User;
import com.repository.EventRepository;
import com.repository.UserRepository;
//import com.service.EventCRUD;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import static org.mockito.Matchers.anyLong;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class TestCrudEvent {

	@Mock
	private EventRepository eventRepository;
	
	@Mock
	private UserRepository userRepository;
	
	@InjectMocks
//	private EventCRUD eventCrud;
	
	private User user1;
	private Event event1;
	private Event event2;
	
	@Before
	public void before() {
		user1 = new User("asd@asd", "bob", "boby");
		event1 = new Event("a", new Date(), null, "asd@asd");
		event2 = new Event("b", new Date(), new Date(), "asd@asd");
		event1.setEventId(1L);
		event2.setEventId(2L);
		/*
		Employe e = new Employe();
        e.setNom("Raphael");
        e.setTauxHoraire(10);
        when(employeRepo.findEmploye("Raphael")).thenReturn(e);
        payeService.faitLaPaye("Raphael", 10);
        verify(employeRepo, times(2)).findEmploye(anyString())*/
	}
	
	@Test
	public void testGetEventById() {
		System.out.println(event1.toString());
		Long eventId = 1L;
		when(eventRepository.findOne(event1.getEventId())).thenReturn(event1);
		Event eventTest = eventRepository.findOne(eventId);
		assertEquals(eventTest, event1);
		verify(eventRepository).findOne(anyLong());
	}
	
	@Ignore
	@Test
	public void testGetEventByEmail() {
		System.out.println(event1.toString());
		Long eventId = 1L;
		when(eventRepository.findBy_emailUser(event1.get_email()))/*.thenReturn(event1.get_email().indexOf(0))*/;
		Event eventTest = eventRepository.findOne(eventId);
		assertEquals(eventTest, event1);
		verify(eventRepository).findOne(anyLong());
	}

}
