package com.exemple;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.transaction.Transactional;

import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import static org.junit.Assert.*;

import com.domaine.Event;
import com.domaine.User;
import com.repository.EventRepository;
import com.repository.UserRepository;

//TODO transformer Mockito Test
public class ExampleEventAndUser {

	private static final Logger log = LoggerFactory.getLogger(ExampleEventAndUser.class);
	
    @Autowired
    private UserRepository _repositoryUser;
    
    @Autowired
    private EventRepository _repositoryEvent;
    
    @Transactional
    public void demoUser() {
		List<User> users = new ArrayList<>();
    	users.add(0, new User("asd@asd", "bob", "boby"));
    	users.add(1, new User("asd@asda", "boba", "bobya"));
    	log.info("Users Saved");
    	_repositoryUser.save(users.get(0));
		_repositoryUser.save(users.get(1));
		assertTrue(_repositoryUser.count() == 2);
		
		log.info("Users found");
        int i=0;
		for (User user : _repositoryUser.findAll()) {
			assertEquals(users.get(i++), user);
        }
        
		log.info("Specific User found");
        User user = _repositoryUser.findOne(2L);
        assertEquals(users.get(1), user);
        
        log.info("User found with findBy_Email():");
        List<User> listUser = _repositoryUser.findBy_email("asd@asda");
        assertTrue(listUser.size() == 1);
        assertEquals(users.get(1), listUser.get(0));
	 }
	/*
	@Transactional
    public void demoEvent() {
		log.info("--------------------------------------------");
		_repositoryEvent.save(new Event(new Date(), null, 1L));
		_repositoryEvent.save(new Event(new Date(), null, 2L));

		Date dateTest = new Date();
		_repositoryEvent.save(new Event(dateTest, null, 1L));
		
		log.info("----------------------------------------------------------------------------------------------");
		log.info("Event found with findAll():");
        for (Event events : _repositoryEvent.findAll()) {
            log.info(events.toString());
        }
        log.info("");
        
        Event event = _repositoryEvent.findOne(2L);
        log.info("--------------------------------");
        log.info("Event found with findOne():");
        log.info(event.toString());
        log.info("");
        
        log.info("--------------------------------------------");
        log.info("Event found with findBy_EventDate():");
        for (Event bauer : _repositoryEvent.findBy_eventDateDebut(dateTest)) {
            log.info(bauer.toString());
        }
	 }
	
	@Transactional
    public void demoUserEvent() {
		log.info("--------------------------------------------");
		for (User users : _repositoryUser.findAll()) {
			log.info("<------------>");
			Long userId = users.getId();
			for (Event event : _repositoryEvent.findBy_userId(userId)) {
	            users.addEvent(event);
	            _repositoryUser.save(users);
	        }
		}
		for (User users : _repositoryUser.findAll()) {
            log.info(users.toString());
        }
	 }
	 */
}
