import React from 'react'
import { Switch, Route } from 'react-router-dom'
import LoginForm from './component/LoginForm.jsx';
import RegisterForm from './component/RegisterForm.jsx';
import Settings from './component/Settings.jsx';
import Home from './Home.js';
import Disgusting from './component/Disgusting.jsx';
import CreateEventForm from './component/CreateEventForm.jsx';
import ModifyEventForm from './component/ModifyEventForm.jsx';
import ConfirmationForm from './component/ConfirmationRegister.jsx';
import ConfirmationActivation from './component/ConfirmationActivation.jsx';
import ConfirmationActivation2 from './component/ConfirmationActivation.jsx';

const Main = () => (
  <main>
    <Switch>
      <Route exact path='/' component={LoginForm}/>
      <Route path='/home' component={Home}/>
      <Route path='/settings' component={Settings}/>
      <Route path='/eventCreation' component={CreateEventForm}/>
      <Route path='/eventModification' component={ModifyEventForm}/>
      <Route path='/register' component={RegisterForm}/>
      <Route path='/disgusting' component={Disgusting}/>
      <Route path='/registerConfirmation' component ={ConfirmationForm}/>
      <Route path='/ActiverMonCompte' component ={ConfirmationActivation2}/>
      <Route path='/ActiverCompte/:id' component ={ConfirmationActivation}/>
    </Switch>
  </main>
)

export default Main
