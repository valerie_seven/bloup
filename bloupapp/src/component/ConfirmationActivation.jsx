import React from "react";
import axios from "axios";
import $ from 'jquery';

export default class ConfirmationRegister extends React.Component {
  constructor(props) {
    super(props);
    const {id} = this.props.match.params;
    if(id !== ""){
    	axios.post("/Register/ActivateUser/", $.param({    	
      	tmpid: id,
    	}))
    	this.props.history.push("/ActiverMonCompte");
    }       
  }

  
  render() {
    return (
   		 <div class="top-content">
            <div class="inner-bg">
                <div class="container">
                    <div class="row">
                        <div class="col-sm-6 col-sm-offset-3 form-box">
                        	<div class="form-top">
                        		<div class="form-top-left">
                        			<h2>Bienvenue sur bloup!</h2>
                        			<h3>Votre Compte a ete active</h3><br/>
                        		</div>
                        		<div class="form-top-right">
                        			<img src="assets/ico/favicon.jpg" alt="HTML5 Icon" width="128" height="128"></img>
                        		</div>
                            </div>
                            <div class="form-bottom">
			                    <form action="/" method="get" class="login-form">
			                        <button type="submit" class="btn">Se connecter</button>
			                    </form>
		                    </div>
                        </div>
                    </div>
                </div>
            </div>
            
        </div>
    );
  }
}