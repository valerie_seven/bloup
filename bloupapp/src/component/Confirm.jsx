import React from 'react';
import {Dialog, FlatButton} from "material-ui";

export default class Confirm extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            open: false
        }
    }

    render() {
        return (
            this.state.open ?
                <Dialog title={this.props.title}
                        actions={this.formatActions()}
                        modal={!!this.props.modal}
                        open={this.state.open}
                        onRequestClose={this.close.bind(this)}
                >
                    {this.props.children}
                </Dialog> : null
        );
    }

    formatActions() {
        return (this.props.actions ? this.props.actions.map((action) => {
            return <FlatButton
                {...action}
            />
        }) : null);
    }

    open() {
        this.setState({open: true})

    }

    close() {
        this.setState({open: false})
    }
}