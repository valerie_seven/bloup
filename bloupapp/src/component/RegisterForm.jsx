import React from "react";
import axios from "axios";
import $ from 'jquery';
 
export default class RegisterForm extends React.Component {
   errorStyle = {
      color: 'red',
      backgroundcolor:'black',
    };
  constructor(props) {
    super(props);
    this.state = {
        email: '',
        password: '',
        checkPassword: '',
        lastName: '',
        firstName: '',
        errorMessage: ''
    };
    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
    
  }
 
  handleChange(event) {
	  const target = event.target;
    const value = target.value;
    const name = target.name;
 
    this.setState({
      [name]: value
    });
  }
 
  handleSubmit(event) {
    event.preventDefault();
    axios.post("/User/createUser", $.param({
      email: this.state.email,
      password: this.state.password,
      passconf: this.state.checkPassword,
      lastName: this.state.lastName,
      firstName: this.state.firstName
    })).then((response) => {
      if(response.data === "")
         this.props.history.push("/registerConfirmation");
         this.setState({errorMessage:response.data});
      this.forceUpdate();
    });    
  }
  
  render() {
    return (
    <div class="top-content">
              <div class="inner-bg">
                  <div class="container">
                      <div class="row">
                          <div class="col-sm-6 col-sm-offset-3 form-box">
                            <div class="form-top">
                            <div class="form-top-left">
                              <h3>Rejoindre la famille Bloup</h3>
                                <p>Entrez vos informations</p>
                            </div>
                            <div class="form-top-right">
                              <img src="assets/ico/favicon.jpg" alt="HTML5 Icon" width="128" height="128" />
                            </div>
                              <p style={this.errorStyle}> {this.state.errorMessage} </p>
                            </div>
                            <div class="form-bottom">
                          <form action="" onSubmit={this.handleSubmit} method="post" class="login-form">
                            <div class="form-group">
                              <label class="sr-only" for="email">Email</label>
                                <input type="text" name="email" value={this.state.email} onChange={this.handleChange} placeholder="monCourriel@Courriel.com" class="form-username form-control" id="form-username" />
                              </div>
                              <div class="form-group">
                                <label class="sr-only" for="password">Mot de passe</label>
                                <input type="password" name="password" value={this.state.password} onChange={this.handleChange} placeholder="Mot de passe" class="form-password form-control" id="form-password" />
                              </div>
			                        <div class="form-group">
                                <label class="sr-only" for="checkPassword">Confirmation du Mot de passe</label>
                                <input type="password" name="checkPassword" value={this.state.checkPassword} onChange={this.handleChange} placeholder="Confirmer mot de passe" class="form-password form-control" id="form-password" />
                              </div>
                              <div class="form-group">
                              <label class="sr-only" for="firstName">Prenom</label>
                                <input type="text" name="firstName" value={this.state.firstName} onChange={this.handleChange} placeholder="Prenom" class="form-username form-control" id="form-username" />
                              </div>
                              <div class="form-group">
                              <label class="sr-only" for="lastName">Nom</label>
                                <input type="text" name="lastName" value={this.state.lastName} onChange={this.handleChange} placeholder="Nom" class="form-username form-control" id="form-username" />
                              </div>
                              <button type="submit" class="btn">Creer mon compte!</button>
                          </form>
                          <form action="/" method="get" class="login-form">
	                        <button type="submit" class="btn">Retour au login</button>
		                  </form>
                        </div>
                      </div>
                  </div>
              </div>
          </div>
      </div>
    );
  }
}