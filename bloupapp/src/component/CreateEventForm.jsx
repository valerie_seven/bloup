import React from "react";
import axios from 'axios';
import $ from 'jquery';
import {FlatButton, DatePicker, TimePicker} from "material-ui";
import moment from 'moment';

export default class CreateEventForm extends React.Component {

  errorStyle = {
      color: 'red',
      backgroundcolor:'black',
    };
	constructor(props) {
	    super(props);
	    this.state = {
	    		title: '',
	    		startDatePicker: moment().toDate(),
	    		startTimePicker: '',
	    		endTimePicker: '',
	    		errorMessage: ''
	    };
	}

	setEvent() {
		if (!this.state.startTimePicker) {
			let date = moment(this.state.startDatePicker);
			axios.post("/Event/createBAE/", $.param({
				userEmail: localStorage.getItem('email'),
				title: this.state.title,
				eventStartingDate: date.utc().format("YYYY-MM-DD"),
			}))
			.then(response => {
				if(!response.data){
					this.setState({errorMessage: "Erreur, l'événement n'a pu être créé. L'heure de fin doit être supérieure à l'heure de début."})
					this.forceUpdate();
				} else {
					var event={title: this.state.title, start: date, end: this.state.eventEndingDate};
					this.props.onSubmit(event);
				}
			})
			.catch((error) => {
				console.log("error",error)
			})
		} else {
			let date = moment(this.state.startDatePicker);
			let time = moment(this.state.startTimePicker);
			let dateAndTime = date.hours(time.hours()).minutes(time.minutes());
			let endDate = '';

      if(this.state.endTimePicker instanceof Date){
        if (this.state.endTimePicker == '' || this.state.endTimePicker == null || this.state.endTimePicker == undefined) {
          endDate = null;
        } else{
          let endTime = moment(this.state.endTimePicker);
          endDate = endTime.year(date.year()).month(date.month()).date(date.date());
          endDate = endDate.utc().format("YYYY-MM-DDTHH:mm");
        }
      }

			axios.post("/Event/createBE/", $.param({
				userEmail: localStorage.getItem('email'),
				title: this.state.title,
        eventStartingDate: dateAndTime.utc().format("YYYY-MM-DDTHH:mm"),
				eventEndingDate: endDate
			}))
			.then(response => {
				if(!response.data){
					this.setState({errorMessage: "Erreur, l'événement n'a pu être créé. L'heure de fin doit être supérieure à l'heure de début"})
					this.forceUpdate();
				} else {
					var event={title: this.state.title, start: dateAndTime, end: this.state.eventEndingDate};
					this.props.onSubmit(event);
				}
			})
			.catch((error) => {
				console.log("error",error)
			})
		}
	}

	handleSubmit(event) {
		event.preventDefault();
		this.setEvent();
	}

	render() {
		return (
			<div className="form-bottom">
				<form action="" onSubmit={this.handleSubmit.bind(this)}>

                    	<p style={this.errorStyle}> {this.state.errorMessage} </p>

						<div className="form-group">
							<label htmlFor="eventTitle">Titre</label><br/>
							<input required type="text" name="eventTitle" value={this.state.title} onChange={(e) => {this.setState({title:e.target.value})}} placeholder="Titre de l'événement" id="form-eventTitle" />
						</div>

						<div className="form-group">
							<label htmlFor="eventStartingDate">Date de début</label><br/>
							<DatePicker required hintText="Choisissez une date" value={this.state.startDatePicker} autoOk onChange={(a, date) => {this.setState({startDatePicker: date})}} />
							<TimePicker required hintText="Heure de début" autoOk minutesStep={5} onChange={(a, date) => {this.setState({startTimePicker: date})}} />
						</div>

						<div className="form-group">
							<label htmlFor="eventEndingDate">Heure de fin (Optionel)</label><br/>
							<TimePicker hintText="Heure de fin" autoOk minutesStep={5}  defaultValue={null} defaultOpenValue={null} onChange={(a, date) => {this.setState({endTimePicker: date})}} />
						</div>

			      		<FlatButton primary label="Créer mon événement!" type="submit"/>
				</form>
			</div>
		);
  }
}
