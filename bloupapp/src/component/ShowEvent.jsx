import React from "react";
import axios from 'axios';
import $ from 'jquery';
import {FlatButton, DatePicker, TimePicker} from "material-ui";
import moment from 'moment';

export default class ShowEvent extends React.Component {
    errorStyle = {
      color: 'red',
      backgroundcolor:'black',
    };
	constructor(props) {
		super(props);
		this.state = {
			title: this.props.title,
			startDatePicker: this.props.startDatePicker,
			startTimePicker: this.props.startDatePicker,
			id: this.props.id,
			endTimePicker: {},
			errorMessage: ''
		};
	}

	componentDidMount(){
		if (null != this.props.endingDate) {
			this.setState({endTimePicker: this.props.endingDate._d})
		}
	}

	deleteEvent() {
		axios.post("/Event/deleteEvent/", $.param({
			eventId: this.props.id,
		}))
		.then(response => {
			if(!response.data){
				this.setState({errorMessage: "Erreur, l'événement n'a pu être suprimé"})
				this.forceUpdate();
			} else {
				this.props.deleteEventFromCalendar(this.props._id);
			}
		})
		.catch((error) => {
			console.log("error", error)
		})
	}

	updateEvent() {
		let date = moment(this.state.startDatePicker);
		let time = moment(this.state.startTimePicker);
		let dateAndTime = date.hours(time.hours()).minutes(time.minutes())

		let endDate = null;
    if(this.state.endTimePicker instanceof Date){
      if (this.state.endTimePicker == '' || this.state.endTimePicker == null || this.state.endTimePicker == undefined) {
        endDate = null;
      } else{
        let endTime = moment(this.state.endTimePicker);
        endDate = endTime.year(date.year()).month(date.month()).date(date.date());
        endDate = endDate.utc().format("YYYY-MM-DDTHH:mm");
      }
    }

		axios.post("/Event/updateEvent/", $.param({
			eventId: this.props.id,
			title: this.state.title,
			eventStartingDate: dateAndTime.utc().format("YYYY-MM-DDTHH:mm"),
			eventEndingDate: endDate,
			isRepetitive: false,
			repetitionType: 0,
			comment: "",
			userEmail: this.props.email
		}))
		.then(response => {
			if(!response.data){
				this.setState({errorMessage: "Erreur, l'heure de fin doit être supérieure à l'heure de début"})
				this.forceUpdate();
			} else {
				this.props.updateEvent();
			}
		})
		.catch((error) => {
			console.log("error", error)
		})
	}

	handleSubmit(event) {
		event.preventDefault();
		this.updateEvent();
	}

	render() {
	    return (
	    	<div className="form-bottom">
				<form action="" onSubmit={this.handleSubmit.bind(this)}>

                    	<p style={this.errorStyle}> {this.state.errorMessage} </p>

						<input type="hidden" name="eventId" value=""/>
						<div className="form-group">
							<label htmlFor="eventId">Titre</label><br/>
							<input type="text" name="eventId" value={this.state.title} onChange={(e) => {this.setState({title:e.target.value})}} />
						</div>

						<div className="form-group">
							<label htmlFor="eventStartingDate">Date de début</label><br/>
							<DatePicker required hintText="Choisissez une date" autoOk value={this.state.startDatePicker} onChange={(a, date) => {this.setState({startDatePicker: date})}} />
							<TimePicker hintText="Temps (Optionel)" autoOk minutesStep={5} value={this.state.startTimePicker} onChange={(a, date) => {this.setState({startTimePicker: date})}} />
						</div>

						<div className="form-group">
							<label htmlFor="eventEndingDate">Heure de fin (Optionel)</label><br/>
							<TimePicker hintText="Heure de fin" autoOk minutesStep={5} value={this.state.endTimePicker} onChange={(a, date) => {this.setState({endTimePicker: date})}} />
						</div>

			      		<FlatButton primary label="Sauvegarder les modifications" type="submit"/>
			      		<FlatButton primary label="Suprimer" onClick={this.deleteEvent.bind(this)}/>
				</form>
			</div>
	    );
	}
}
