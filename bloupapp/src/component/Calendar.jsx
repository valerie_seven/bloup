import React from 'react';
import axios from 'axios';
import $ from 'jquery';
import 'fullcalendar';
import '../../node_modules/fullcalendar/dist/fullcalendar.css';
import '../../node_modules/fullcalendar/dist/locale/fr-ca.js';
import CreateEventForm from './CreateEventForm'
import ShowEvent from "./ShowEvent"
import Confirm from './Confirm'
import {FlatButton} from "material-ui";


class Calendar extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      dialogBody: ""
    };
  }

  componentDidMount(){
    const { calendar } = this.refs;

    $(calendar).fullCalendar({
      dayClick: (date, jsEvent, view) => {
        this.refs.dialog.open();
        if (view.type === "month") {
          this.refs.createForm.setState({startDatePicker: date.add(1, "day").toDate()});
        } else {
          this.refs.createForm.setState({startDatePicker: date.toDate()});
        }
      },
      header: {
        left: 'prev,next today',
        center: 'title',
        right: 'month,agendaWeek,agendaDay,listWeek'
      },
      defaultDate: new Date(),
      navLinks: true,
      editable: false,
      eventStartEditable: false,
      eventLimit: true,
      events: [],
          eventClick: (calEvent, jsEvent, view) => {            this.setState({
              dialogBody: <ShowEvent updateEvent={this.updateEvent.bind(this)} deleteEventFromCalendar={this.deleteEventFromCalendar.bind(this)}
                title={calEvent.title} startDatePicker={calEvent.start._d} id={calEvent.id} _id={calEvent._id} endingDate={calEvent.end}
                email={localStorage.getItem('email')}/>
            })
            this.refs.showEventDialog.open();
          }
    });

    this.getEvent();
  }

  handleClick(e) {
      this.refs.dialog.open();
    }

  getEvent() {
    axios.post("/Event/getByUser", $.param({
      userEmail: localStorage.getItem('email')
    }))
        .then(response => {
          response.data.forEach(function(element) {
            var id = element.eventId;
            var date = new Date(element.eventStartingDate);
            if (element.eventEndingDate == null) {
              var event={id: id, title: element._title, start:  date};
            } else {
              var endingDate = new Date(element.eventEndingDate);
              var event = {id: id, title: element._title, start:  date, end: endingDate};
            }
            $('#calendar').fullCalendar( 'renderEvent', event, true);
          });
        })
        .catch((error) => {
          console.log("error",error)
        })
  }

  render() {
    const divStyle = {
      backgroundColor: "#eee",
      padding: "2px",
      width: "70%",
      margin: "auto",
      marginTop: "2%",
      borderRadius: "10px",
      float: "left"
    };

    return (
      <div style={divStyle}>
      <div ref='calendar' id='calendar'></div>
      <FlatButton primary label="Ajouter un événement" onClick={this.handleClick.bind(this)} />
      <Confirm ref='dialog' title="Créer un événement">
        <CreateEventForm ref='createForm' onSubmit={this.onSubmit.bind(this)}/>
      </Confirm>
      <Confirm ref='showEventDialog' title="Détails de l'événement">
        {this.state.dialogBody}
      </Confirm>
      </div>
    );
  }

  onSubmit(event) {
    $('#calendar').fullCalendar('removeEvents');
    this.getEvent();
    this.refs.dialog.close();
  }

  updateEvent(){
    $('#calendar').fullCalendar('removeEvents');
    this.getEvent();
    this.refs.showEventDialog.close();
  }

  deleteEventFromCalendar(id) {
    $('#calendar').fullCalendar('removeEvents', id);
    $('#calendar').fullCalendar('refetchEvents');
    this.refs.showEventDialog.close();
  }
}

export default Calendar;
