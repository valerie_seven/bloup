import React from "react";
import {FlatButton} from "material-ui";
import {withRouter} from 'react-router-dom';
import axios from 'axios';
import $ from 'jquery';

const CommentForm = ({addComment}) => {
  	const inputStyle = {
    	width: "100%"
	};

  	let input;

  	return (
    	<div>
      		<input type="text" style={inputStyle} placeholder="Votre Note" name="comment" ref={node => { input = node; }} />
      		<FlatButton primary onClick={() => { addComment(input.value); input.value = ''; }}>
      			<span className="glyphicon glyphicon-plus"></span>
      		</FlatButton>
    	</div>
  	);
}

const Comment = ({comment, remove}) => {
	const subText = {
    			color: "#aaa",
    			fontFamily: "verdana",
    			fontSize: "8px"
	};

  	return (
  		<li className="media">
  			<div className="media-left">
           		<img src="assets/ico/favicon.jpg" alt="HTML5 Icon" width="30" height="100%" />
            </div>
            <div className="media-body">
  				<p>{comment._comment}</p>
  				<span style={subText}>{comment._date}</span>
  			</div>
  			<div className="media-right" >
  				<FlatButton primary onClick={() => { remove(comment._id); }}>
      				<span className="glyphicon glyphicon-trash"></span>
      			</FlatButton>
            </div>
  		</li>
  	);
}

const CommentList = ({comments, remove}) => {
	const ulStyle = {
	};
  	const commentNode = comments.map((comment) => {
    	return (<Comment comment={comment} key={comment._id} remove={remove}/>)
  	});
  	return (<ul className="media-list" style={ulStyle}>{commentNode}</ul>);
}

const Title = () => {
  	return (
    	<div>
       		<div className="panel-heading">
          		<h3 className="panel-title">
                	<span className="glyphicon glyphicon-comment"></span> 
                    Notes
                </h3>
       		</div>
    	</div>
  	);
}

class Comments extends React.Component {

	constructor(props) {
    	super(props);
    	this.state = {
    		data: []
    	};
  	}

    addComment(val){
    	if(val !== ""){
	    	axios.post("/Comment/createComment/", $.param({
				comment: val,
				email: localStorage.getItem('email')
			}))
      		.then(response => {
      			console.log(response.data);
				if(!response.data){
					alert('Erreur, le commentaire na pas pu etre creer');
				} else {
          this.props.history.push("/disgusting");
          this.forceUpdate();
				}
      		})
			.catch((error) => {
					console.log("error",error)
			})
		}
  	}

  	handleRemove(id){

    	axios.post("/Comment/deleteComment/", $.param({
			id: id
		}))
      		.then(response => {
		        console.log(response.data);
				if(!response.data){
					alert('Erreur, le commentaire na pas pu etre enlever');
				} else {
          this.props.history.push("/disgusting");
					this.forceUpdate();
				}
      		})
			.catch((error) => {
					console.log("error",error)
			})
  	}

    componentDidMount(){
    	axios.post("/Comment/getByEmail/", $.param({
			     email: localStorage.getItem('email')
		  }))
  		.then(response => {
        this.setState({data:response.data});
  		});
  	}

	render() {

	    return (
			<div className="panel panel-default" >
                <Title />
				<div className="panel-body" >
                    <CommentList
          				comments={this.state.data}
          				remove={this.handleRemove.bind(this)}
        			/>
                    <CommentForm addComment={this.addComment.bind(this)}/>
                </div>
           </div>
		);
	}
}
export default withRouter(Comments);
