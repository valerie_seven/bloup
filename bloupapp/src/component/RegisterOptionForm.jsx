import React from "react";
import { Link } from 'react-router-dom'

export default class RegisterOptionForm extends React.Component {
	constructor(props) {
	    super(props);

	    this.handleSubmit = this.handleSubmit.bind(this);
	  }
	
	handleSubmit(event) {
	    alert('Register');
	    event.preventDefault();
	}
	
	render() {
	    return (
	      <form onSubmit={this.handleSubmit} className="login-form">
			<Link to='/register'><button type="submit" className="btn">Creer un compte</button></Link>
		  </form>
	    );
	  }
}