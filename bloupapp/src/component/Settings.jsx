import React from 'react';
import {FlatButton} from "material-ui";
import { Link, withRouter } from 'react-router-dom';
import axios from 'axios';
import $ from 'jquery';

class Settings extends React.Component {

	constructor(props) {
    	super(props);
    	this.state = {
    		firstName: localStorage.getItem('firstName'),
    		lastName: localStorage.getItem('lastName'),
    		email: localStorage.getItem('email'),
    		currentPWD: '',
    		newPWD: '',
    		confirmPWD: '',
    		errorMessagePWD: '',
    		errorMessageName: ''
    	};

    	this.handleChange = this.handleChange.bind(this);
  	}

	handleChange(event) {
	  const target = event.target;
	  const value = target.value;
	  const name = target.name;

	  this.setState({
		  [name]: value
	  });
  	}

	handleClickUpdateName(e) {
		e.preventDefault();
     	axios.post("/User/updateUserName/", $.param({
			email: localStorage.getItem('email'),
			firstName: this.state.firstName,
			lastName: this.state.lastName
		}))
      		.then(response => {
      			console.log(response.data);
				if(!response.data){
					alert('Erreur, changement nom na pas pu etre fait');
				} else {
      				this.setState({errorMessageName:response.data});
      				localStorage.setItem('firstName', this.state.firstName);
    				localStorage.setItem('lastName', this.state.lastName);
				}
      		})
			.catch((error) => {
					console.log("error",error)
			})
    }

    handleClickUpdatePassword(e) {
    	e.preventDefault();
     	axios.post("/Password/updatePassword/", $.param({
			email: localStorage.getItem('email'),
    		currentPWD: this.state.currentPWD,
    		newPWD: this.state.newPWD,
    		confirmPWD: this.state.confirmPWD,
		}))
      		.then(response => {
      			console.log(response.data);
				if(!response.data){
					alert('Erreur, changement password na pas pu etre fait');
				} else {
					this.setState({errorMessagePWD:response.data});
					this.setState({currentPWD:''});
					this.setState({newPWD:''});
					this.setState({confirmPWD:''});
				}
      		})
			.catch((error) => {
					console.log("error",error)
			})
    }

	render() {

		const divStyle = {
			      backgroundColor: "#eee",
			      padding: "10px",
			      width: "30%",
			      margin: "auto",
			      marginTop: "2%",
			      borderRadius: "10px"
		};

		const inputStyle = {
    		width: "100%"
		};

	    return (
	    	<div className="container" style={divStyle}>
	    		<h1>Profil</h1>
	    		<br/>
	    		<div>
	    			<h2>Email</h2>
	    			<h3>{this.state.email}</h3>
	    			<hr/>
	    		</div>
	    		<div display="block">
	    			<h2>Nom</h2>
	    			<p style={this.errorStyle}> {this.state.errorMessageName} </p>
	    			<div>
	    				<input type="text" style={inputStyle} name="firstName" value={ this.state.firstName } onChange={this.handleChange} />
	    			</div>
	    			<div>
	    				<input type="text" style={inputStyle} name="lastName" value={ this.state.lastName } onChange={this.handleChange} />
	    			</div>
	    			<FlatButton primary label="Modifier" onClick={this.handleClickUpdateName.bind(this)} />
	    			<hr/>
	    		</div>
	    		<div>
	    			<h2>Mot de passe</h2>
                    <p style={this.errorStyle}> {this.state.errorMessagePWD} </p>
	    			<div>
	    				<input type="password" style={inputStyle} name="currentPWD"  placeholder="Actuel" value={ this.state.currentPWD } onChange={this.handleChange} />
	    			</div>
	    			<div>
	    				<input type="password" style={inputStyle} name="newPWD" placeholder="Nouveau" value={ this.state.newPWD } onChange={this.handleChange} />
	    			</div>
	    			<div>
	    				<input type="password" style={inputStyle} name="confirmPWD" placeholder="Confirmer Nouveau" value={ this.state.confirmPWD } onChange={this.handleChange}/>
	    			</div>
	    			<FlatButton primary label="Modifier" onClick={this.handleClickUpdatePassword.bind(this)} />
	    			<hr/>
	    		</div>
	    		<Link to='/home'><FlatButton primary label="Retour" /></Link>
       		</div>
		);
	}
}
export default withRouter(Settings);
