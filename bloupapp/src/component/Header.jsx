import React from "react";
import axios from 'axios';

export default class Header extends React.Component {
	 divStyle = {
		color: "black",
		backgroundColor: "#eee",
		padding: "2px",
		width: "70%",
		margin: "auto",
		marginTop: "2%",
		borderRadius: "10px",
		float: "left"
	};
	constructor(props) {
	    super(props);
	    this.state = { DidyouknowFact: '' };
	}

	componentDidMount(){
		axios.post("/Fact/Didyouknow")
				.then(response => {
					this.setState({DidyouknowFact: response.data});
				})
				.catch((error) => {
					console.log("error",error)
				})
	}

	render() {
		return (
				<div style={this.divStyle}>
					<h1> Saviez-vous que? </h1>
					<h2>{this.state.DidyouknowFact}</h2>;
				</div>
	    );
	  }
}
