import React from 'react';
import Comments from './Comments.jsx';
import {FlatButton} from "material-ui";
import { Link, withRouter } from 'react-router-dom';

class Menu extends React.Component {
	constructor(props) {
   		 super(props);
    console.log(props);
  	}

    handleClickSettings(e) {
    	this.props.history.push("/settings");
    }

	render() {
		const divStyle = {
			      backgroundColor: "#eee",
			      padding: "10px",
			      width: "26%",
			      margin: "auto",
			      marginTop: "2%",
			      borderRadius: "10px",
			      float: "right"
		};
	    return (
	    	<div className="container" style={divStyle}>
	    		<div className="media">
	    			<div className="media-body">
	    				<h1>{localStorage.getItem('firstName')}  {localStorage.getItem('lastName')}</h1>
	    			</div>
	    			<img src="assets/ico/favicon.jpg"  className="img-circle align-self-center" alt="HTML5 Icon" width="13%" height="10%" />
                </div>
				<FlatButton primary label="Profil" onClick={this.handleClickSettings.bind(this)} />
				<Link to='/'><FlatButton primary label="Deconnexion" /></Link>
				<Comments />
       		</div>
		);
	}
}
export default withRouter(Menu);
