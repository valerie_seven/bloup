import React from "react";

export default class ConfirmationRegister extends React.Component {
  constructor(props) {
    super(props);
  }

  
  render() {
    return (
   		 <div class="top-content">
            <div class="inner-bg">
                <div class="container">
                    <div class="row">
                        <div class="col-sm-6 col-sm-offset-3 form-box">
                        	<div class="form-top">
                        		<div class="form-top-left">
                        			<h2>Bienvenue sur bloup!</h2>
                        			<h3>Vos information ont été enregistré avec succès!</h3><br/>
                        			<h3>Il ne reste plus qu'a activer votre compte en cliquant sur le lien de confirmation envoyé a votre email.</h3>
                        		</div>
                        		<div class="form-top-right">
                        			<img src="assets/ico/favicon.jpg" alt="HTML5 Icon" width="128" height="128"></img>
                        		</div>
                            </div>
                            <div class="form-bottom">
			                    <form action="/" method="get" class="login-form">
			                        <button type="submit" class="btn">Se connecter</button>
			                    </form>
		                    </div>
                        </div>
                    </div>
                </div>
            </div>
            
        </div>
    );
  }
}