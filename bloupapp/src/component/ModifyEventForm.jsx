import React from "react";
import { Link } from 'react-router-dom'
import axios from 'axios';
import $ from 'jquery';

export default class ModifyEventForm extends React.Component {
	constructor(props) {
    super(props);
    this.state = {
    		eventId: '',
    		eventStartingDate: '',
    		eventEndingDate: ''
    };
	}

  getEvent() {
    axios.post("/Event/getById/", $.param({
			eventId: this.state.eventId
		}))
				.then(response => {
					if(!response.data){
						alert('Erreur, levenement na pas pu etre trouver');
					}

					this.setState({
							eventStartingDate:(new Date(response.data.eventStartingDate)).toISOString().slice(0,16),
					});
					if(response.data.eventEndingDate !== null){
							this.setState({
									eventEndingDate:(new Date(response.data.eventEndingDate)).toISOString().slice(0,16)
							});
					}

				})
    }

	setEvent() {
		axios.post("/Event/sUpdateEvent/", $.param({
			userEmail: "asd@asd.com",
			eventId: this.state.eventId,
			eventStartingDate: this.state.eventStartingDate,
			eventEndingDate: this.state.eventEndingDate
		}))
				.then(response => {
					if(!response.data){
						alert('Erreur, levenement na pas pu etre modifier');
					}
				})
				.catch((error) => {
					console.log("error",error)
				})
	}

	handleSubmit(event) {
		event.preventDefault();
		if((this.state.eventStartingDate === "")){
			this.getEvent();
		}else{
			this.setEvent();
			this.props.history.push("/calendar");
		}
	}

	render() {
		return (
			<div class="form-bottom">

				<form action="" onSubmit={this.handleSubmit.bind(this)}>

						<div class="form-group">
							<label class="sr-only" for="eventId">Id Evenement</label>
							<input type="text" name="eventId" value={this.state.eventId} onChange={(e) => {this.setState({eventId:e.target.value})}} placeholder="Id Evenement" id="form-eventTitle" />
						</div>

						<div class="form-group">
							<label class="sr-only" for="eventStartingDate">Date Evenement Debut</label>
							<input type="datetime-local" name="eventStartingDate" value={this.state.eventStartingDate}
								onChange={(e) => {this.setState({eventStartingDate:e.target.value})}} />
						</div>

						<div class="form-group">
							<label class="sr-only" for="eventEndingDate">Date Evenement Fin</label>
							<input type="datetime-local" name="eventEndingDate" value={this.state.eventEndingDate}
								onChange={(e) => {this.setState({eventEndingDate:e.target.value})}} />
						</div>

						{/* Email User */}

						<Link to='/calendar'>a</Link>
							<button type="submit" class="btn">Modifier mon Evenement!</button>
				</form>
			</div>
		);
  }
}
