import React from "react";
import axios from "axios";
import $ from 'jquery';
import RegisterOptionForm from './RegisterOptionForm.jsx';

axios.default.withCredentials = true;

export default class LoginForm extends React.Component {
  errorStyle = {
      color: 'red',
      backgroundcolor:'black',
    };
  constructor(props) {
    super(props);
    this.state = {
    		email: '',
    		password: '',
    		errorMessage: ''
    };

    localStorage.setItem('email', "");
    localStorage.setItem('firstName', "");
   	localStorage.setItem('lastName', "");

    var id = this.getParameter("id");
	if(id !== ""){
		this.props.history.push("/ActiverCompte/" + id);
    }
    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  handleChange(event) {
	  const target = event.target;
	  const value = target.value;
	  const name = target.name;

	  this.setState({
		  [name]: value
	  });
  }
  getParameter(key) {
    let value = "";
    let params = window.location.search.split("&");
    params[0] = params[0].substr(1);
    params.forEach((p) => {
	let param = p.split("=");
      if (param[0] === key) {
      value = param[1];
      }
    })

    return decodeURI(value);
  }

  handleSubmit(event) {
    event.preventDefault();

    axios.post("/Login/", $.param({
      email: this.state.email,
      password: this.state.password,
    })).then((response) => {
    	if(response.data !== ""){
    	localStorage.setItem('email', response.data.email);
    	localStorage.setItem('firstName', response.data.firstName);
    	localStorage.setItem('lastName', response.data.lastName);
    		this.props.history.push("/home");
    	}
    	else{
    		this.setState({errorMessage:"Le mot de passe ou le email est invalide"});
	    	this.forceUpdate();
    	}
    });
  }

  render() {
    return (
    	<div className="top-content">
	    	<div className="inner-bg">
	        	<div className="container">
	            	<div className="row">
	                	<div className="col-sm-6 col-sm-offset-3 form-box">
	                    	<div className="form-top">
	                        	<div className="form-top-left">
	                        		<h3>Bienvenue sur bloup</h3>
	                        		<p>Entrez votre nom d'utilisateur et votre mot de passe</p>
	                        	</div>
	                        	<div className="form-top-right">
	                        		<img src="assets/ico/favicon.jpg" alt="HTML5 Icon" width="128" height="128" />
	                        	</div>
	                        	<p style={this.errorStyle}> {this.state.errorMessage} </p>
	                        </div>
	                        <div className="form-bottom">
	                        	<form onSubmit={this.handleSubmit}  className="login-form">
      								<div className="form-group">
      									<label className="sr-only" htmlFor="username">Email</label>
      									<input type="text" name="email" value={this.state.email} onChange={this.handleChange} placeholder="email" className="form-username form-control" id="form-username"/>
       								</div>
      								<div className="form-group">
      									<label className="sr-only">Mot de passe</label>
      									<input type="password" name="password" value={this.state.password} onChange={this.handleChange} placeholder="mot de passe" className="form-username form-control" id="form-username" />
        							</div>
      								<button type="submit" className="btn">Se connecter</button>
      							</form>
	                        	<RegisterOptionForm />
	                        </div>
	                    </div>
	                </div>
	            </div>
	        </div>
	    </div>
    );
  }
}
