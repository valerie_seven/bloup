import React, { Component } from 'react';
import Footer from './component/Footer.jsx';
import Main from './Main.js';
import 'bootstrap-css'
import {MuiThemeProvider} from 'material-ui';


class App extends Component {
  render() {
    return (
        <MuiThemeProvider>
      	<div>
		    	<Main />
		    	<Footer />
		    </div>
        </MuiThemeProvider>
    );
  }
}

export default App;
