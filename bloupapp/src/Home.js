import React from 'react';
import Header from './component/Header.jsx';
import Calendar from './component/Calendar.jsx';
import Menu from './component/Menu.jsx';

export default class Home extends React.Component {
	constructor(props) {
  		 super(props);
 	}
	
	render() {
	    return (
	    	<div className="container" display="table-cell">
				<Calendar />
				<Menu />
				<Header />
       		</div>
		);
	}
}
